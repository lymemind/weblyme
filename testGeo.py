import GEOparse
import pandas as pd
from pandas import DataFrame
import os
import sys
import traceback


def testGeo(geoId):
    try:
        print (geoId)
        gse = GEOparse.get_GEO(geo = str(geoId), destdir="./")
        print("GSM example:")
    
        for gsm_name, gsm in gse.gsms.items():
            print("Name: ", gsm_name)
            print("Metadata:",)
            for key, value in gsm.metadata.items():
                print(" - %s : %s" % (key, ", ".join(value)))
            #print ("Table data:",)
            #print (gsm.table.head())
            
            break
        
        #print()
        #print("GPL example:")
        
        #for gpl_name, gpl in gse.gpls.items():
            
            #print("Name: ", gpl_name)
            #print("Metadata:",)
            
            #for key, value in gpl.metadata.items():
                #print(" - %s : %s" % (key, ", ".join(value)))
            
            #print("Table data:",)
            #print(gpl.table.head())
            #print (len(gpl.table))
            
            #break
    except:
        traceback.print_exc(file=sys.stdout)

    return

def readGeo():

    try:
        f = open('LymeGEO.txt', 'r')
        geoIDList = []
        for line in f:
            if line.find('Accession: GSE') != -1:
                geoId = "".join(line[line.index('Accession: GSE') + 10:line.index('Accession: GSE') + 19])
                print (" geo ID = " + str(geoId))
                
                geoIDList.append(geoId.replace("\t",""))
                
                #testGeo(str(geoId))  
                #break
        print (geoIDList)
        
        for gid in geoIDList:
            testGeo(str(geoId))  
            
        #[' GSE84479', ' GSE45996', ' GSE63085', ' GSE55815', ' GSE50074', ' GSE50075', ' GSE27252', ' GSE27995', ' GSE6055', ' GSE68741', ' GSE16195', ' GSE6092', ' GSE26968', ' GSE31740']        
    except:
        traceback.print_exc(file=sys.stdout)

    return

readGeo()
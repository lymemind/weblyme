import pandas as pd

import os
import sys
import traceback

import io
import re
import shutil

from django.conf import settings
import numpy
import itertools
import csv
import datetime
import os.path

from io import StringIO
import random
import csv
from collections import OrderedDict
from datetime import datetime
import time
from math import log
import json
from utils import *
import seaborn as sns

def createDf1():
    
    try:

        fileNames = ["~/self/lyme/mbio_paper/gsea_results_v1.tsv","~/self/lyme/mbio_paper/gsea_results_v2.tsv" ,"~/self/lyme/mbio_paper/gsea_results_v5.tsv" ]
        
        dfs = [pd.read_table(fileName,sep='\t', index_col=None) for fileName in fileNames]

        outFileNames = ["~projects/webLyme/outdfs/v1_df.csv","~projects/webLyme/outdfs/v2_df.csv", "~projects/webLyme/outdfs/v5_df.csv"]
        directions = ["up", "down", "all"]

        dbPathwayMap = {}

        for index, df in enumerate ( dfs ) :
            
            fileName = fileNames[index]
            
            for indexData, row in df.iterrows():
                
                #print (" index for file " + str(fileName) + " value = " + str(indexData) )
                
                database = row[".id"]
                
                if database not in dbPathwayMap:
                    dbPathwayMap[database] = {}
                    
                if fileName not in dbPathwayMap[database]:
                    dbPathwayMap[database][fileName] = {}
                    
                pathway = row["module"] + "-" + row["feature"]
                
                adjPValue = float(row["Adjusted.Pvalue"])
                geneList = row["enrichment_overlap"].split("|")
                
                if pathway not in dbPathwayMap[database][fileName]:
                    dbPathwayMap[database][fileName][pathway] = (adjPValue, geneList) 

        outDfRowList = []
        
        geneSymbolDF = pd.read_table("genelistmapping.txt",sep='\t', index_col=None)
        geneSymbolDF["entrezID"] = geneSymbolDF["entrezID"].astype(int)        
        entrezIDList = geneSymbolDF["entrezID"].tolist()
        geneSymbolList = geneSymbolDF["geneSymbol"].tolist()

        #{
          #"key": "Iraq",
          #"region": "Asia",
          #"subregion": "Western Asia",
          #"value": 34035000
        #},

        dataList = []
        for database, dbMap in dbPathwayMap.items():
            
            print (database)
            #print (dbMap)
    
            #outDf = pd.DataFrame(columns=["fileName", "Pathway1", "Pathway2", "Genes1", "Genes2"], index = None)

            for fileName, fileMap in dbMap.items():
                
                #print (fileName)
                #print (fileMap)
                
                fileMap2 = dbMap[fileName]
                
                for pathway, dataTuple in fileMap.items():
                    
                        (adjPValue, geneList) = dataTuple
                        
                        for gene in geneList:
                            
                            data = {"key":geneSymbolList[entrezIDList.index(int(gene))], "region":fileName[-6:-4], "subregion":pathway, "value":int(np.round(-1*np.log10(adjPValue))) }
                            dataList.append(data)
                        
            outf = open("outdfs/treemap/treemapData_"+database+".json", "w")
            outf.write(str(dataList))
            outf.close()
        
    except:
        
        traceback.print_exc(file=sys.stdout)
        
    return

createDf1()



%matplotlib inline
import pandas as pd
import os
import sys
import traceback
import numpy
import seaborn as sns
import matplotlib.pyplot as plt

fileName = "~/self/lyme/rnaseq_counts/gene_counts.tsv"
df = pd.read_table(fileName,sep='\t', index_col=None) 
geneId = 'IL6'
df1 = df[df['Geneid'] == 'IL6']
df1.iloc[0].tolist()[1:]

dfnew = pd.DataFrame(index=None, columns=['Expression', 'SampleId','Status', 'Visit'])
dfnew['Expression'] = df1.iloc[0].tolist()[1:]
dfnew['SampleId'] = df.columns[1:]
dfnew = dfnew[(dfnew['SampleId'].str.find('JHU-01')!= -1) | (dfnew['SampleId'].str.find('JHU-02')!= -1)]
dfnew['Status'] = [ 'Control' if x.find('JHU-01')!= -1 else 'Case' for x in dfnew['SampleId'] ] 

dfnew['Visit'] = [x[-1:] for x in dfnew['SampleId']]
dfnew = dfnew[dfnew['Visit'].isin(['1','2','5'])]
fig = plt.figure()
fig.patch.set_facecolor('white')
sns.boxplot(x="Visit", y="Expression", hue="Status", data=dfnew, palette="PRGn")
sns.boxplot(x="Visit", y="Expression",  data=dfnew, palette="PRGn")
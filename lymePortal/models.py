from django.db import models
from django.contrib.auth.models import User

class Project(models.Model):
    name = models.CharField(max_length=256)
    user = models.ForeignKey(User, blank = True, null=True)
    description = models.CharField(max_length=512, blank = True, null = True)
    def __str__(self):
        return self.name
    
class Datafile(models.Model):
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=512, blank = True, null = True)
    project = models.ForeignKey ( Project )     
    def __str__(self):
        return self.name
    
class FileType(models.Model):
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=512)
    def __str__(self):
        return self.name

#class JobStatusCode(models.Model):
    #code = models.CharField(max_length=10)
    #description = models.CharField(max_length=255, null=True)
    #def __unicode__(self):
        #return self.code

#class SubmittedJobType(models.Model):
    #name = models.CharField ( max_length=10)  
    #description = models.CharField ( max_length=255)   
    #def __unicode__(self):
        #return str(self.description)     
    
#class AdjustMethod(models.Model):
    #name = models.CharField(max_length=256)
    #description =  models.CharField(max_length=512)
    #def __str__(self):
        #return self.name
        
#class NormalizationMethod(models.Model):
    #name = models.CharField ( max_length=10)  
    #description = models.CharField ( max_length=255)   
    #def __unicode__(self):
        #return str(self.description) 

#class AnalysisDetail(models.Model):
    
    #name = models.CharField ( max_length=255, blank = True, null=True)

    #dataFile = models.ForeignKey(DataFile) 
    
    #phenotypeFile = models.ForeignKey(DataFile, related_name = "phenotypeFile")     
    
    #description = models.CharField ( max_length=255)

    #sampleColumnName = models.CharField ( max_length=255)  
    #blockColumnName = models.CharField ( max_length=255, blank = True, null=True)       

    #logFoldChangeDecideTests = models.FloatField(blank = True, null=True)
    #pValueCutOffDecideTests = models.FloatField(blank = True, null=True)
    
    #normalizationMethod = models.ForeignKey(NormalizationMethod, blank = True, null=True)

    #def __unicode__(self):
        #return str(self.dataFile)

#class AnalysisResultFile(models.Model):
 
    #name = models.CharField ( max_length=128)  
    #filePath = models.CharField ( max_length=255)
    #analysisDetail = models.ForeignKey(AnalysisDetail) 
    
    #resultFileName = models.CharField ( max_length=128)      
    
    #def __unicode__(self):
        #return str(self.description) 

#class AnalysisPlot(models.Model):
 
    #name = models.CharField ( max_length=128)  
    #plotPath = models.CharField ( max_length=255)

    #analysisDetail = models.ForeignKey(AnalysisDetail) 
    
    #plotFileName = models.CharField ( max_length=128)  
    
    #def __unicode__(self):
        #return str(self.description) 
    
#class SubmittedJob(models.Model):
    #name = models.CharField(max_length=512)
    #description = models.CharField(max_length=512, null=True, blank = True)
    #submittedBy = models.ForeignKey(User) 
    #submittedOn = models.DateTimeField( null=True, blank = True)
    #submittedJobType = models.ForeignKey(SubmittedJobType)
    #jobStatusCode = models.ForeignKey(JobStatusCode)
    #analysisDetail = models.ForeignKey(AnalysisDetail, null=True, blank = True)
    #completedTime = models.DateTimeField(null=True, blank=True)
    #def __unicode__(self):
        #return self.name    

#class ColumnType(models.Model):
    #name = models.CharField(max_length=256)
    #description = models.CharField(max_length=512)
    #def __str__(self):
        #return self.name

class LymeDenormalizedPmids(models.Model):
    term_1 = models.CharField(max_length=767)
    id1 = models.IntegerField()
    term_2 = models.CharField(max_length=767)
    id2 = models.IntegerField()
    pmid = models.IntegerField()
    title = models.CharField(max_length=600)
    journal = models.CharField(max_length=300)
    submission_date = models.DateField(blank=True, null=True)
    impact_factor = models.FloatField(blank=True, null=True)
    relevance_score = models.FloatField(blank=True, null=True)
    table_id = models.AutoField(primary_key=True)
    class Meta:
        db_table = 'lyme_denormalized_pmids'
        managed = False        

class LymeOmni(models.Model):
    #id = models.IntegerField(blank=True, null=True)
    indication = models.CharField(max_length=255, blank=True, null=True)
    entrez_gene_id = models.BigIntegerField(blank=True, null=True)
    gene_symbol = models.CharField(max_length=255, blank=True, null=True)
    tissue = models.CharField(max_length=45, blank=True, null=True)
    val = models.FloatField(blank=True, null=True)
    val_type = models.CharField(max_length=255, blank=True, null=True)
    source_table = models.CharField(max_length=255, blank=True, null=True)
    feature = models.CharField(max_length=255, blank=True, null=True)
    groups = models.CharField(max_length=255, blank=True, null=True)
    class Meta:
        db_table = 'lyme_omni'
        managed = False        

class LymeOmniDrugbankTargets(models.Model):
    id = models.AutoField(primary_key=True)
    drug_bank_id = models.CharField(max_length=255, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    type = models.CharField(max_length=255, blank=True, null=True)
    uni_prot_id = models.CharField(max_length=255, blank=True, null=True)
    uni_prot_name = models.CharField(max_length=255, blank=True, null=True)
    entrez_gene_id = models.IntegerField(blank=True, null=True)
    gene_symbol = models.CharField(max_length=255, blank=True, null=True)
    organism = models.CharField(max_length=255, blank=True, null=True)
    class Meta:
        db_table = 'lyme_omni_drugbank_targets'
        managed = False        

class LymeOmniEqtls(models.Model):
    chr = models.IntegerField(blank=True, null=True)
    probe = models.CharField(max_length=255, blank=True, null=True)
    gene_symbol = models.CharField(max_length=255, blank=True, null=True)
    entrez_gene_id = models.IntegerField(blank=True, null=True)
    tss = models.BigIntegerField(blank=True, null=True)
    tse = models.BigIntegerField(blank=True, null=True)
    snp_id = models.CharField(max_length=255, blank=True, null=True)
    snp_info = models.FloatField(blank=True, null=True)
    snp_coor = models.BigIntegerField(blank=True, null=True)
    a1 = models.CharField(max_length=255, blank=True, null=True)
    freq_a1 = models.FloatField(blank=True, null=True)
    fat_beta = models.FloatField(blank=True, null=True)
    fat_sebeta = models.FloatField(blank=True, null=True)
    fat_p = models.FloatField(blank=True, null=True)
    lcl_beta = models.FloatField(blank=True, null=True)
    lcl_sebeta = models.FloatField(blank=True, null=True)
    lcl_p = models.FloatField(blank=True, null=True)
    skin_beta = models.FloatField(blank=True, null=True)
    skin_se_beta = models.FloatField(blank=True, null=True)
    skin_p = models.FloatField(blank=True, null=True)
    table_id = models.AutoField(primary_key=True)
    class Meta:
        db_table = 'lyme_omni_eqtls'
        managed = False        

class LymeOmniFisherScores(models.Model):
    entrez_gene_id = models.BigIntegerField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    fisher_score = models.FloatField(blank=True, null=True)
    fisher_type = models.IntegerField(blank=True, null=True)    
    table_id = models.AutoField(primary_key=True)
    class Meta:
        db_table = 'lyme_omni_fisher_scores'
        managed = False        

class LymeOmniHgnc(models.Model):
    id = models.AutoField(primary_key=True)
    hgnc_id = models.TextField(blank=True, null=True)
    gene_symbol = models.TextField(blank=True, null=True)
    approved_name = models.TextField(blank=True, null=True)
    synonyms = models.TextField(blank=True, null=True)
    entrezgeneid = models.IntegerField(blank=True, null=True)
    entrezgeneid_hgnc = models.TextField(blank=True, null=True)
    class Meta:
        db_table = 'lyme_omni_hgnc'
        managed = False        

class LymeOmniIndex(models.Model):
    table_id = models.AutoField(primary_key=True)
    entrez_gene_id = models.BigIntegerField(blank=True, null=True)
    gene_symbol = models.CharField(max_length=255, blank=True, null=True)
    meta_p = models.FloatField(blank=True, null=True)
    fat_p = models.FloatField(blank=True, null=True)
    lcl_p = models.FloatField(blank=True, null=True)
    skin_p = models.FloatField(blank=True, null=True)
    proteomics_p = models.FloatField(blank=True, null=True)
    v1_p = models.FloatField(blank=True, null=True)
    v2_p = models.FloatField(blank=True, null=True)
    v5_p = models.FloatField(blank=True, null=True)
    gene_nlp_p = models.FloatField(blank=True, null=True)
    disease_nlp_p = models.FloatField(blank=True, null=True)
    chemical_nlp_p = models.FloatField(blank=True, null=True)
    sea_p = models.FloatField(blank=True, null=True)
    drugbank_p = models.FloatField(blank=True, null=True)
    combined_p_meta_skin_prot = models.FloatField(blank=True, null=True)
    combined_p_meta_skin = models.FloatField(blank=True, null=True)
    combined_p_meta_skin_prot_wildcard = models.FloatField(blank=True, null=True)
    class Meta:
        db_table = 'lyme_omni_index'
        managed = False        

class LymeOmniListofv1IngenuityUniqEid(models.Model):
    gene_symbol = models.TextField(blank=True, null=True)
    entrez_gene_id = models.IntegerField(blank=True, null=True)
    table_id = models.AutoField(primary_key=True)
    class Meta:
        db_table = 'lyme_omni_listofv1_ingenuity_uniq_eid'
        managed = False        

class LymeOmniListofv2IngenuityUniqEid(models.Model):
    gene_symbol = models.TextField(blank=True, null=True)
    entrez_gene_id = models.IntegerField(blank=True, null=True)
    table_id = models.AutoField(primary_key=True)
    class Meta:
        db_table = 'lyme_omni_listofv2_ingenuity_uniq_eid'
        managed = False        

class LymeOmniListofv5IngenuityUniqEid(models.Model):
    gene_symbol = models.TextField(blank=True, null=True)
    entrez_gene_id = models.IntegerField(blank=True, null=True)
    table_id = models.AutoField(primary_key=True)
    class Meta:
        db_table = 'lyme_omni_listofv5_ingenuity_uniq_eid'
        managed = False        

class LymeOmniMetaLymeCasevsctrlV2(models.Model):
    indication = models.CharField(max_length=255, blank=True, null=True)
    tissue = models.CharField(max_length=255, blank=True, null=True)
    groups = models.CharField(max_length=255, blank=True, null=True)
    entrez_gene_id = models.IntegerField(blank=True, null=True)
    multi_gene_symbols = models.CharField(max_length=255, blank=True, null=True)
    meta_effect_size = models.FloatField(blank=True, null=True)
    meta_effect_size_se = models.FloatField(blank=True, null=True)
    meta_p = models.FloatField(blank=True, null=True)
    meta_qv = models.FloatField(blank=True, null=True)
    n_samp_1 = models.IntegerField(blank=True, null=True)
    n_samp_2 = models.IntegerField(blank=True, null=True)
    n_studies = models.IntegerField(blank=True, null=True)
    table_id = models.AutoField(primary_key=True)
    multi_entrez_gene_ids = models.CharField(max_length=255, blank=True, null=True)
    gene_symbol = models.CharField(max_length=255, blank=True, null=True)
    class Meta:
        db_table = 'lyme_omni_meta_lyme_casevsctrl_v2'
        managed = False        

class LymeOmniNlp(models.Model):
    id1 = models.IntegerField()
    id2 = models.IntegerField()
    coocurrence_count = models.IntegerField()
    pvalue = models.FloatField()
    adjpvalue = models.FloatField()
    oddsratio = models.FloatField(blank=True, null=True)
    term_1 = models.CharField(max_length=767)
    term_1_count = models.IntegerField()
    term_1_type = models.CharField(max_length=20)
    term1_relation_source = models.CharField(max_length=200, blank=True, null=True)
    dbid_1 = models.CharField(max_length=200)
    dbid_1_species = models.CharField(max_length=100, blank=True, null=True)
    dbid_1_type = models.CharField(max_length=100)
    term_2 = models.CharField(max_length=767)
    term_2_count = models.IntegerField()
    term_2_type = models.CharField(max_length=20)
    term2_relation_source = models.CharField(max_length=200, blank=True, null=True)
    dbid_2 = models.CharField(max_length=200)
    dbid_2_species = models.CharField(max_length=100, blank=True, null=True)
    dbid_2_type = models.CharField(max_length=100)
    relation = models.CharField(max_length=50, blank=True, null=True)
    score = models.FloatField(blank=True, null=True)
    pmid = models.IntegerField(blank=True, null=True)
    relationship_id = models.CharField(max_length=101, blank=True, null=True)
    table_id = models.AutoField(primary_key=True)
    class Meta:
        db_table = 'lyme_omni_nlp'
        managed = False        

class LymeOmniNlpIdToPmidMap(models.Model):
    table_id = models.AutoField(primary_key=True)
    term_id = models.IntegerField(blank=True, null=True)
    pmid = models.IntegerField(blank=True, null=True)
    title_count = models.IntegerField(blank=True, null=True)
    body_count = models.IntegerField(blank=True, null=True)
    tf_idf = models.FloatField(blank=True, null=True)
    class Meta:
        db_table = 'lyme_omni_nlp_id_to_pmid_map'
        managed = False        

class LymeOmniNlpMeta(models.Model):
    id = models.AutoField(primary_key=True)
    id1 = models.IntegerField()
    id2 = models.IntegerField()
    coocurrence_count = models.IntegerField()
    pvalue = models.FloatField()
    adjpvalue = models.FloatField()
    oddsratio = models.FloatField(blank=True, null=True)
    term_1 = models.CharField(max_length=767)
    term_1_count = models.IntegerField()
    term_1_type = models.CharField(max_length=20)
    term1_relation_source = models.CharField(max_length=200, blank=True, null=True)
    dbid_1 = models.CharField(max_length=200)
    dbid_1_species = models.CharField(max_length=100, blank=True, null=True)
    dbid_1_type = models.CharField(max_length=100)
    term_2 = models.CharField(max_length=767)
    term_2_count = models.IntegerField()
    term_2_type = models.CharField(max_length=20)
    term2_relation_source = models.CharField(max_length=200, blank=True, null=True)
    dbid_2 = models.CharField(max_length=200)
    dbid_2_species = models.CharField(max_length=100, blank=True, null=True)
    dbid_2_type = models.CharField(max_length=100)
    relation = models.CharField(max_length=50, blank=True, null=True)
    score = models.FloatField(blank=True, null=True)
    pmid = models.IntegerField(blank=True, null=True)
    relationship_id = models.CharField(max_length=101, blank=True, null=True)
    class Meta:
        db_table = 'lyme_omni_nlp_meta'
        managed = False        

class LymeOmniNlpPmidData(models.Model):
    #id = models.AutoField(primary_key=True)
    pmid = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=600)
    journal = models.CharField(max_length=300)
    submission_date = models.DateField(blank=True, null=True)
    impact_factor = models.FloatField(blank=True, null=True)
    citations = models.IntegerField(blank=True, null=True)
    class Meta:
        db_table = 'lyme_omni_nlp_pmid_data'
        managed = False        

class LymeOmniProteomics(models.Model):
    indication = models.TextField(blank=True, null=True)
    tissue = models.TextField(blank=True, null=True)
    grps = models.TextField(blank=True, null=True)
    uni_prot_id = models.TextField(blank=True, null=True)
    entrez_gene_id = models.IntegerField(blank=True, null=True)
    protein_name = models.TextField(blank=True, null=True)
    mean_diff = models.FloatField(blank=True, null=True)
    p = models.FloatField(blank=True, null=True)
    table_id = models.AutoField(primary_key=True)
    class Meta:
        db_table = 'lyme_omni_proteomics'
        managed = False        

class LymeOmniSeaTargets(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    entrez_gene_id = models.IntegerField(primary_key=True)
    gene_symbol = models.CharField(max_length=255, blank=True, null=True)
    organism = models.CharField(max_length=255, blank=True, null=True)
    class Meta:
        db_table = 'lyme_omni_sea_targets'
        managed = False        

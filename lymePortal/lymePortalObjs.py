class GeneDetailObj(object):
    def __init__(self):

        self.fileName = ''

        self.entrezId = ''
        self.geneSymbol = ''
        
        self.drugBankLink = ''
        
        self.metaP = ''
        self.metaPLink = ''

        self.fatP = ''
        self.skinP = ''
        self.lclP = ''
        
        self.fatPLink = False
        self.skinPLink = False
        self.lclPLink = False
        
        self.v1Flag = False
        self.v2Flag = False
        self.v5Flag = False
        
        self.chemicalNLPLink = False
        self.diseaseNLPLink = False
        self.geneNLPLink = False
        self.seaLink = False

        self.proteomics = ''
        self.proteomicsLink = False

        self.combinedPMetaSkinProteomic = ''
        self.combinedPMetaSkin = ''
        self.combinedPMetaSkinProteomicGt2 = ''
        
        self.evidenceLink = False
        
        self.validRecord = True
        
    def __unicode__(self):
        return str(self.fileName) 
    
class ContrastObj(object):
    def __init__(self):

        self.contrast = ''
        self.contrastId = 0

        self.geneUpObjList = []
        self.geneDownObjList = []

        self.cutOffDownLogFC = -1.5
        self.cutOffDownPValue = 0

        self.cutOffUpLogFC = 1.5
        self.cutOffUpPValue = 0
        
        self.scatterPlotPath = ''
        self.scatterPlotName = ''
        
        self.clusterMapName = ''    
        
        self.bokehHeatMapPlotScript = ''
        self.bokehHeatMapPlotDiv = ''        

    def __unicode__(self):
        return str(self.contrast)
    
class GeneInfoObj(object):
    def __init__(self):

        self.geneId = ''
        self.geneSymbol = 0

        self.logFC = 0
        self.tValue = 0
        self.pValue = 0
        self.adjustedPValue = 0

    def __unicode__(self):
        return str(self.geneId)
    
class DatafileObj(object):
    def __init__(self):

        self.datafileName = ''
        self.columns = 0
        self.dataMatrix = 0

    def __unicode__(self):
        return str(self.datafileName)    
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from lymePortal.views import *

admin.autodiscover()

urlpatterns = [

    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', landing, name = 'landing'),

    url(r'^lymePortal/$', landing, name = 'landing'),
    
    url(r'^lymePortal/processLanding/$', processLanding, name = 'processLanding'),
    url(r'^lymePortal/listAngularProjects/$', listAngularProjects, name = 'listAngularProjects'),

    url(r'^lymePortal/listProjects/$', listProjects, name = 'listProjects'),
    url(r'^lymePortal/listDatafiles/$', listDatafiles, name = 'listDatafiles'),
    url(r'^lymePortal/listDatabases/$', listDatabases, name = 'listDatabases'),

    url(r'^lymePortal/listSequenceProjects/$', listSequenceProjects, name = 'listSequenceProjects'),
    url(r'^lymePortal/displayProjectData/$', displayProjectData, name = 'displayProjectData'),
    url(r'^lymePortal/addProject/$', addProject, name = 'addProject'),
    url(r'^lymePortal/submitAddProject/$', submitAddProject, name = 'submitAddProject'),
    
    url(r'^lymePortal/visualizationOptions/$', visualizationOptions, name = 'visualizationOptions'),
    url(r'^lymePortal/visualizeData/$', visualizeData, name = 'visualizeData'),
    url(r'^lymePortal/displayFileList/$', displayFileList, name = 'displayFileList'),
    url(r'^lymePortal/displayLimmaResults/$', displayLimmaResults, name = 'displayLimmaResults'),
    url(r'^lymePortal/displayGeneDetails/$', displayGeneDetails, name = 'displayGeneDetails'),
    url(r'^lymePortal/getProjectByName/$', getProjectByName, name = 'getProjectByName'),
    url(r'^lymePortal/showBoxPlots/$', showBoxPlots, name = 'showBoxPlots'),

    url(r'^lymePortal/fetchOmniDetail/$', fetchOmniDetail, name = 'fetchOmniDetail'),

    url(r'^lymePortal/fetchOmniDetail/w+$', fetchOmniDetail, name = 'fetchOmniDetail'),
    url(r'^lymePortal/literatureHome/$', literatureHome, name = 'literatureHome'),
    
    url(r'^lymePortal/fetchPmidDetail/$', fetchPmidDetail, name = 'fetchPmidDetail'),    
    
    url(r'^lymePortal/processLiteratureLanding/$', processLiteratureLanding, name = 'processLiteratureLanding'),
    url(r'^lymePortal/primaryLiteratureHome/$', primaryLiteratureHome, name = 'primaryLiteratureHome'),
    url(r'^lymePortal/secondaryLiteratureHome/$', secondaryLiteratureHome, name = 'secondaryLiteratureHome'),
    url(r'^lymePortal/evidenceMatrixHome/$', evidenceMatrixHome, name = 'evidenceMatrixHome'),

    url(r'^lymePortal/processDataLanding/$', processLiteratureLanding, name = 'processDataLanding'),
    url(r'^lymePortal/fetchGeoData/$', fetchGeoData, name = 'fetchGeoData'),
    url(r'^lymePortal/submitFetchGeoData/$', submitFetchGeoData, name = 'submitFetchGeoData'),

    url(r'^lymePortal/submitAddDataFile/$', submitAddDataFile, name = 'submitAddDataFile'),
    url(r'^lymePortal/addDataFile/$', addDataFile, name = 'addDataFile'),
    
    url(r'^lymePortal/dataHome/$', literatureHome, name = 'dataHome'),
    url(r'^lymePortal/showPlots/$', showPlots, name = 'showPlots'),

    url(r'^lymePortal/processDataLanding/$', processDataLanding, name = 'processDataLanding'),   

    url(r'^lymePortal/submitFetchEvidence/$', submitFetchEvidence, name = 'submitFetchEvidence'),       

    url(r'^lymePortal/feedback/$', feedback, name = 'feedback'),
    url(r'^lymePortal/submitFeedback/$', submitFeedback, name = 'submitFeedback'),
    url(r'^lymePortal/showHelp/$', showHelp, name = 'showHelp'),

    url(r'^lymePortal/register/', register, name = 'register'),
    url(r'^accounts/', include('registration.urls'))

]

urlpatterns += staticfiles_urlpatterns()

#print str(urlpatterns)

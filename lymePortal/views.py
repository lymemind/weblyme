from django.contrib.auth.decorators import login_required
from django.contrib import messages

from django.template import RequestContext
from django.shortcuts import render_to_response
from django.shortcuts import render
from django.http import HttpResponse

from lymePortal.models import *
from lymePortal.lymePortalObjs import *
from lymePortal.lymePortalConstants import *

import pandas as pd
from pandas import DataFrame
import os
import sys
import traceback

import io
import re
import shutil

from bokeh.plotting import *
import bokeh
import matplotlib.colors as colors
from bokeh.plotting import figure, ColumnDataSource

from bokeh.models import (
    ColumnDataSource,
    HoverTool,
    LinearColorMapper,
    BasicTicker,
    PrintfTickFormatter,
    ColorBar,
)

from bokeh.embed import components
from bokeh.models import Range1d

from bokeh.plotting import figure

from django.conf import settings
import numpy
import itertools
import csv
import datetime
import os.path
from django.db.models import Q
from io import StringIO
import random
import csv
from collections import OrderedDict
from datetime import datetime
import time
from math import log
import json
from utils import *
import seaborn as sns
import numbers
from io import StringIO
from django.db import connection
from django.core.mail import send_mail
import matplotlib
matplotlib.use('Agg')

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            return HttpResponseRedirect("/registration/")
    else:
        form = UserCreationForm()
    return render(request, "registration/register.html", {
        'form': form,
    })

#@login_required
def landing(request):
    return render(request, "lymePortal/landing.html", {

    })

#@login_required
#@csrf_protect
def processLanding(request):

    ''' Function process landing page submit
    Input: params.request
    Output: Delegates task to corresponding handler
    '''
    lymePortalHomeButton = request.POST.get("lymePortalHomeButton","0" )
    print(lymePortalHomeButton)

    if lymePortalHomeButton == "0":
        return listProjects ( request )
    elif lymePortalHomeButton == "1" :
        return literatureHome( request )

def processLiteratureLanding(request):

    ''' Function process literature landing page submit
    Input: params.request
    Output: Delegates task to corresponding handler
    '''
    lymePortalLiteratureButton = request.POST.get("lymePortalLiteratureButton","1" )
    print(lymePortalLiteratureButton)

    if lymePortalLiteratureButton == "0":
        return primaryLiteratureHome ( request )
    elif lymePortalLiteratureButton == "1" :
        return secondaryLiteratureHome ( request )
    elif lymePortalLiteratureButton == "2" :
        return evidenceMatrixHome  ( request )

def processDataLanding(request):

    ''' Function process data landing page submit
    Input: params.request
    Output: Delegates task to corresponding handler
    '''
    lymePortalHomeButton = request.POST.get("lymePortalHomeButton","1" )
    print(lymePortalHomeButton)

    if lymePortalDataButton == "0":
        return listProjects ( request )
    elif lymePortalDataButton == "1" :
        return fetchGeoData( request )
    elif lymePortalDataButton == "2" :
        return listSequenceProjects( request )

def literatureHome(request):
    try:
        print ( " in literature home ")
    except:
      traceback.print_exc(file=sys.stdout)
    return render(request, 'lymePortal/literatureHome.html', {
    }, RequestContext(request))

def dataHome(request):
    try:
        print ( " in data home ")
    except:
      traceback.print_exc(file=sys.stdout)
    return render(request, 'lymePortal/dataHome.html', {
    }, RequestContext(request))

def primaryLiteratureHome(request):
    try:

        if request.method == 'GET':

            searchTerm = request.GET.get("searchTerm","" )
            searchTermType = request.GET.get("searchTermType","" )
            searchTermTypes = ["Chemical", "Disease", "Gene"]
            downloadFlag = request.GET.get("downloadFlag","" )

        elif request.method == 'POST':
        
            searchTerm = request.POST.get("searchTerm","" )
            searchTermType = request.POST.get("searchTermType","" )
            searchTermTypes = ["Chemical", "Disease", "Gene"]
            downloadFlag = request.POST.get("downloadFlag","" )
        
        if searchTerm == '' and searchTermType == '':
            pubmedRecordList = LymeOmniNlp.objects.all()
        else:
            if searchTerm != '' and searchTermType != '':
                pubmedRecordList = LymeOmniNlp.objects.filter(term_1__contains = searchTerm, term_1_type =  searchTermType)
            else:
                if searchTerm != '':
                    pubmedRecordList = LymeOmniNlp.objects.filter(term_1__contains = searchTerm)
                elif searchTermType != '':
                    pubmedRecordList = LymeOmniNlp.objects.filter( term_1_type =  searchTermType)
        totalRecCount = len(pubmedRecordList)
        
        if downloadFlag == "1":
            
            response = HttpResponse(content_type='text/csv')
            
            filename = "lyme_omni_nlp_" + str(datetime.now()).replace("-", "")[0:8] + ".csv"
            response["Content-Disposition"] = "attachment; filename=" + filename
        
            writer = csv.writer(response)
            writer.writerow(['Primary Term','Primary Term Type','DBID','Adjusted P Value'])
            
            for rec in pubmedRecordList:
                writer.writerow([rec.term_1, rec.term_1_type, rec.dbid_1, '{:0.4e}'.format(rec.adjpvalue)])
            
            response["Content-type"] = "text/csv"
            return response          
                    
        if len(pubmedRecordList) > 500:
            pubmedRecordList = pubmedRecordList[:500]
        
        pubmedRecords = []        
        
        for rec in pubmedRecordList:
            pubmedRecords.append([rec.term_1, rec.term_1_type, rec.dbid_1, '{:0.4e}'.format(rec.adjpvalue), rec.id1, rec.id2])
	
        print ( len(pubmedRecords) )

        #output = StringIO()            
        #cw = csv.writer(output)
        #output_matrix = []
        #output_matrix.append(['Primary Term','Primary Term Type','DBID','Adjusted P Value'])
        
        #for rec in pubmedRecordList:
            #output_matrix.append([rec.term_1, rec.term_1_type, rec.dbid_1, '{:0.4e}'.format(rec.adjpvalue), rec.id1])
            
        #cw.writerows(output_matrix)
        #output = make_response(output.getvalue())
        #filename = "lyme_omni_nlp_" + str(datetime.now()).replace("-", "")[0:8] + ".csv"
        #output.headers["Content-Disposition"] = "attachment; filename=" + filename

        showingRecCount = 500
        if totalRecCount < 500:
            showingRecCount = totalRecCount  

    except:
        traceback.print_exc(file=sys.stdout)
    return render(request, 'lymePortal/primaryLiteratureHome.html', {
        "pubmedRecords":pubmedRecords,        
        "searchTerm":searchTerm,        
        "searchTermType":searchTermType,        
        "searchTermTypes":searchTermTypes,   
        "totalRecCount":totalRecCount,
        "showingRecCount":showingRecCount,        
    }, RequestContext(request))

def secondaryLiteratureHome(request):
    try:
        
        #searchPrimaryTerm = request.POST.get("searchPrimaryTerm","" )
        #searchPrimaryTermType = request.POST.get("searchPrimaryTermType","" )
        #searchPrimaryTermTypes = ["Chemical", "Disease", "Gene"]
        #searchSecondaryTerm = request.POST.get("searchSecondaryTerm","" )
        #searchSecondaryTermType = request.POST.get("searchSecondaryTermType","" )
        #searchSecondaryTermTypes = ["Chemical", "Disease", "Gene"]
        #downloadFlag = request.POST.get("downloadFlag","" )
        
        if request.method == 'GET':

            searchPrimaryTerm = request.GET.get("searchPrimaryTerm","" )
            searchPrimaryTermType = request.GET.get("searchPrimaryTermType","" )
            searchPrimaryTermTypes = ["Chemical", "Disease", "Gene"]
            searchSecondaryTerm = request.GET.get("searchSecondaryTerm","" )
            searchSecondaryTermType = request.GET.get("searchSecondaryTermType","" )
            searchSecondaryTermTypes = ["Chemical", "Disease", "Gene"]
            downloadFlag = request.GET.get("downloadFlag","" )

        elif request.method == 'POST':        
        
            searchPrimaryTerm = request.POST.get("searchPrimaryTerm","" )
            searchPrimaryTermType = request.POST.get("searchPrimaryTermType","" )
            searchPrimaryTermTypes = ["Chemical", "Disease", "Gene"]
            searchSecondaryTerm = request.POST.get("searchSecondaryTerm","" )
            searchSecondaryTermType = request.POST.get("searchSecondaryTermType","" )
            searchSecondaryTermTypes = ["Chemical", "Disease", "Gene"]
            downloadFlag = request.POST.get("downloadFlag","" )

        print (" downloadFlag " + downloadFlag)
        
        queryMap = {}

        if searchPrimaryTerm != '':
            queryMap['term_1__contains'] = searchPrimaryTerm
        if searchPrimaryTermType != '':
            queryMap['term_1_type'] = searchPrimaryTermType
            
        if searchSecondaryTerm != '':
            queryMap['term_2__contains'] = searchSecondaryTerm
        if searchSecondaryTermType != '':
            queryMap['term_2_type'] = searchSecondaryTermType
            
        pubmedRecordList = LymeOmniNlp.objects.filter(Q(**queryMap))
                    
        pubmedRecords = []        
        totalRecCount = len(pubmedRecordList)
        
        if downloadFlag == "1":
            
            response = HttpResponse(content_type='text/csv')
            
            filename = "pubmed_secondary_" + str(datetime.now()).replace("-", "")[0:8] + ".csv"
            response["Content-Disposition"] = "attachment; filename=" + filename
        
            writer = csv.writer(response)
            writer.writerow(['Primary Term','Primary Term Type','DBID1','Secondary Term', 'Secondary Term Type', 'DBID2','Relation', 'Adjusted P Value'])
            
            for rec in pubmedRecordList:
                writer.writerow([rec.term_1, rec.term_1_type, rec.dbid_1, rec.term_2, rec.term_2_type, rec.dbid_2, rec.relation, '{:0.4e}'.format(rec.adjpvalue)])
            
            response["Content-type"] = "text/csv"
            return response   
        
        if len(pubmedRecordList) > 500:
            pubmedRecordList = pubmedRecordList[:500]
        for rec in pubmedRecordList:
            pubmedRecords.append([rec.term_1, rec.term_1_type, rec.dbid_1, rec.term_2, rec.term_2_type, rec.dbid_2, rec.relation, '{:0.4e}'.format(rec.adjpvalue), rec.id1, rec.id2])
	
        print ( pubmedRecords )
        
        showingRecCount = 500
        if totalRecCount < 500:
            showingRecCount = totalRecCount

    except:
      traceback.print_exc(file=sys.stdout)
    return render(request, 'lymePortal/secondaryLiteratureHome.html', {
        "pubmedRecords":pubmedRecords,        
        "searchPrimaryTerm":searchPrimaryTerm,        
        "searchPrimaryTermType":searchPrimaryTermType,        
        "searchPrimaryTermTypes":searchPrimaryTermTypes, 
        "searchSecondaryTerm":searchSecondaryTerm,        
        "searchSecondaryTermType":searchSecondaryTermType,        
        "searchSecondaryTermTypes":searchSecondaryTermTypes,   
        "totalRecCount":totalRecCount,
        "showingRecCount":showingRecCount,

    }, RequestContext(request))

def submitFetchEvidence(request):
    try:
        entrezId = request.POST.get("entrezId",0 )
        cancelFlag = request.POST.get("cancelFlag","0" )        
        searchTerm = request.POST.get("searchTerm","" )

        sortColumn = request.POST.get("sortColumn","" )
        sortDirection = request.POST.get("sortDirection","" )
        filterString = request.POST.get("filterString","" )
        
        sortLymeEvidence = request.POST.get("sortLymeEvidence","" )
        lymeEvidenceFilter = request.POST.get("lymeEvidenceFilter","" )    
        pageButton = request.POST.get("pageButton","" )   
        pageNumValue = request.POST.get("pageNum",0 )  
        filterList = []
        if filterString != '':
            filterList = filterString.split(",")        

        pageNum = 0
        try:
            pageNum = int(pageNumValue)
        except:
            pass
        
        if cancelFlag == "1":
            return HttpResponseRedirect('/lymePortal/evidenceMatrixHome/')            
        
        omniRecords = LymeOmniIndex.objects.filter(entrez_gene_id = entrezId) 
        geneSymbol = ''
        timePoints = []
        if len(omniRecords) > 0:
            omniRecord = omniRecords[0]
            geneSymbol = omniRecord.gene_symbol            
            if omniRecord.v1_p:
                timePoints.append("V1")
            if omniRecord.v2_p:
                timePoints.append("V2")
            if omniRecord.v5_p:
                timePoints.append("V5")
                
        timePointsString = ",".join(timePoints)
        
        entrezIds = [entrezId]
        print (entrezIds)

        geneSymbolDF = pd.read_table("genelistmapping.txt",sep='\t', index_col=None)
        geneSymbolDF["entrezID"] = geneSymbolDF["entrezID"].astype(int)        
        entrezIDList = geneSymbolDF["entrezID"].tolist()
        geneSymbolList = geneSymbolDF["geneSymbol"].tolist()
        
        genes = [geneSymbolList [ entrezIDList.index(int(x))] for x in entrezIds]

        geneImageMap = {}

        pd.options.mode.chained_assignment = None
        
        countsFile = settings.DATA_FOLDER + "gene_counts.tsv"
        df = pd.read_table(countsFile,sep='\t', index_col=None)
        
        for gene in genes:
            df1 = df[df['Geneid'] == gene]
            df1.iloc[0].tolist()[1:]
            
            dfnew = pd.DataFrame(index=None, columns=['Expression', 'SampleId','Status', 'Visit'])
            dfnew['Expression'] = df1.iloc[0].tolist()[1:]
            dfnew['SampleId'] = df.columns[1:]
            dfnew = dfnew[(dfnew['SampleId'].str.find('JHU-01')!= -1) | (dfnew['SampleId'].str.find('JHU-02')!= -1)]
            dfnew['Status'] = [ 'Control' if x.find('JHU-02')!= -1 else 'Case' for x in dfnew['SampleId'] ] 
            
            dfnew['Visit'] = [x[-1:] for x in dfnew['SampleId']]
            dfnew['Visit'] = ['1' if dfnew['Status'][i] == 'Control' else x for i,x in enumerate(dfnew['Visit']) ]
            
            dfControl2 = dfnew[dfnew['Status'] == 'Control']
            dfControl5 = dfnew[dfnew['Status'] == 'Control']
            
            dfControl2['Visit'] = '2'
            #dfnew = dfnew.append(dfControl2)
            
            dfControl5['Visit'] = '5'
            #dfnew = dfnew.append(dfControl5)
            dfnew = pd.concat([dfnew, dfControl2, dfControl5], ignore_index=True)
            dfnew = dfnew[dfnew['Visit'].isin(['1','2','5'])]
            plt.clf()
            fig = plt.figure()
            fig.patch.set_facecolor('white')
            snsPlot = sns.boxplot(x="Visit", y="Expression", hue="Status", data=dfnew, palette="PRGn")
            imagePath = settings.IMAGE_FOLDER + "plots/"+ gene + "_boxplot.png"
            plt.savefig(imagePath)
            
            dfnew['Expression'] = [str(x) for x in dfnew['Expression']]
            
            boxPlotImage = gene + "_boxplot"
            boxPlotData = dfnew.as_matrix()

            #geneImageMap[gene] = (gene + "_boxplot", dfnew.as_matrix())
            
        limmaMap = {
            settings.DATA_FOLDER + "DE_full_results_v1_csv.csv":"v1_case_control",
            settings.DATA_FOLDER + "DE_full_results_v2_csv.csv":"v2_case_control",
            settings.DATA_FOLDER + "DE_full_results_v5_csv.csv":"v5_case_control",
            }
        
        geneSymbolDF = pd.read_table("genelistmapping.txt",sep='\t', index_col=None)
        geneSymbolDF["entrezID"] = geneSymbolDF["entrezID"].astype(int)        
        entrezIDList = geneSymbolDF["entrezID"].tolist()
        geneSymbolList = geneSymbolDF["geneSymbol"].tolist()    
        
        geneSymbolList = [x.lower() for x in geneSymbolList]
        
        geneObjList = []
        for filePath, fileName in limmaMap.items():
            
            geneObj = GeneDetailObj()
        
            topTableDataFrame = pd.read_csv(filePath, index_col = None)
            
            topTableDataFrame = topTableDataFrame.rename(columns={'Unnamed: 0': 'geneSymbol'})

            print(topTableDataFrame.columns)
            print(genes[0])
            
            geneDf = topTableDataFrame[topTableDataFrame["geneSymbol"] == genes[0] ] 
            
            for index, row in geneDf.iterrows(): 
            
                print(row)
    
                geneObj.fileName = fileName

                geneObj.geneSymbol = row[0]
                
                geneObj.geneId = entrezId 
     
                geneObj.logFC = '{:0.4e}'.format(row[1])
                geneObj.tValue = '{:0.4e}'.format(row[2])
                geneObj.pValue = '{:0.4e}'.format(row[3])
                geneObj.adjustedPValue = '{:0.4e}'.format(row[4])
                
                geneObjList.append(geneObj)
                
        pubmedRecordList = LymeOmniNlp.objects.filter(dbid_2_type = 'entrez', dbid_2 = entrezId)
        pubmedRecords = []
        for rec in pubmedRecordList:
            pubmedRecords.append([rec.term_1, rec.term_1_type, rec.dbid_1, rec.term_2, rec.term_2_type, rec.dbid_2, rec.relation, '{:0.4e}'.format(rec.adjpvalue), rec.id1, rec.id2, rec.term_1.replace("'", r"\'"), rec.term_2.replace("'", r"\'")])
        
        print ( pubmedRecords )  
        
        omniRecords = LymeOmniIndex.objects.filter(entrez_gene_id = entrezId)  
        geneRecords = []
        for omniRecord in omniRecords:
            
            geneRecord = GeneDetailObj()

            geneRecord.entrezId = omniRecord.entrez_gene_id
            geneRecord.geneSymbol = omniRecord.gene_symbol

            if omniRecord.drugbank_p and omniRecord.drugbank_p != 0:
                geneRecord.drugBankLink = True

            if omniRecord.meta_p and omniRecord.meta_p != 0:
                geneRecord.metaP = '{:0.4e}'.format(omniRecord.meta_p)                
                geneRecord.metaPLink = True

            if omniRecord.fat_p and omniRecord.fat_p != 0:
                geneRecord.fatP = '{:0.4e}'.format(omniRecord.fat_p)                
                geneRecord.fatPLink = True
    
            if omniRecord.skin_p and omniRecord.skin_p != 0:
                geneRecord.skinP = '{:0.4e}'.format(omniRecord.skin_p)                
                geneRecord.skinPLink = True    
        
            if omniRecord.lcl_p and omniRecord.lcl_p != 0:
                geneRecord.lclP = '{:0.4e}'.format(omniRecord.lcl_p)               
                geneRecord.lclPLink = True    
            
            if omniRecord.proteomics_p and omniRecord.proteomics_p and omniRecord.proteomics_p != 0:
                geneRecord.proteomics = '{:0.4e}'.format(omniRecord.proteomics_p)                
                geneRecord.proteomicsLink = True    

            if omniRecord.v1_p and omniRecord.v1_p != 0:
                geneRecord.v1Flag = True                

            if omniRecord.v2_p and omniRecord.v2_p != 0:
                geneRecord.v2Flag = True                
    
            if omniRecord.v5_p and omniRecord.v5_p != 0:
                geneRecord.v5Flag = True  
                
            if geneRecord.v1Flag or geneRecord.v2Flag or geneRecord.v5Flag:
                geneRecord.evidenceLink = True
    
            if omniRecord.chemical_nlp_p and omniRecord.chemical_nlp_p != 0:
                geneRecord.chemicalNLPLink = True                
        
            if omniRecord.disease_nlp_p and omniRecord.disease_nlp_p != 0:
                geneRecord.diseaseNLPLink = True                
            
            if omniRecord.gene_nlp_p and omniRecord.gene_nlp_p != 0:
                geneRecord.geneNLPLink = True                
                
            if omniRecord.sea_p and omniRecord.sea_p != 0:
                geneRecord.seaLink = True        
                
            if omniRecord.combined_p_meta_skin_prot and omniRecord.combined_p_meta_skin_prot != 0:
                geneRecord.combinedPMetaSkinProteomic = omniRecord.combined_p_meta_skin_prot
                
            if omniRecord.combined_p_meta_skin_prot_wildcard and omniRecord.combined_p_meta_skin_prot_wildcard != 0:
                geneRecord.combinedPMetaSkinProteomicGt2 = omniRecord.combined_p_meta_skin_prot_wildcard
                
            if omniRecord.combined_p_meta_skin and omniRecord.combined_p_meta_skin != 0:
                geneRecord.combinedPMetaSkin = omniRecord.combined_p_meta_skin
                
            geneRecords.append(geneRecord)
    
    except:
      traceback.print_exc(file=sys.stdout)
    return render(request, 'lymePortal/submitFetchEvidence.html', {
        "entrezId":entrezId,        
        "geneSymbol":geneSymbol,        
        "timePointsString":timePointsString,  
        "boxPlotImage":boxPlotImage,
        "boxPlotData":boxPlotData,
        "geneObjList":geneObjList,
        "pubmedRecords":pubmedRecords,
        "geneRecords":geneRecords,
        
        "searchTerm":searchTerm,
        "sortColumn":sortColumn,
        "sortDirection":sortDirection,
        "filterString":filterString, 
        "filterList":filterList,
        "pageNum":pageNum,
        
        "sortLymeEvidence":sortLymeEvidence,
        "lymeEvidenceFilter":lymeEvidenceFilter,
        "pageButton":pageButton,
       

    }, RequestContext(request))        

def evidenceMatrixHome(request):
    try:

        print ( " in evidence matrix home ")

        #searchTerm = request.POST.get("searchTerm","" )
        #sortColumn = request.POST.get("sortColumn","" )
        #sortDirection = request.POST.get("sortDirection","" )
        #filterString = request.POST.get("filterString","" )
        #sortLymeEvidence = request.POST.get("sortLymeEvidence","" )
        #lymeEvidenceFilter = request.POST.get("lymeEvidenceFilter","" )    
        #pageButton = request.POST.get("pageButton","" )   
        #pageNumValue = request.POST.get("pageNum",0 )  
        downloadFlag = request.POST.get("downloadFlag","" )        

        pageNum = 0
        try:
            pageNum = int(pageNumValue)
        except:
            pass
        print ( " page num = " + str(pageNum))
        
        #print ( " page num = " + str(pageNum))
        #print ( " page button = " + str(pageButton))
        
        #print ( " sort column = " + str(sortColumn))
        #print ( " sort direction = " + str(sortDirection))
        #print ( " filter String = " + str(filterString))
        
        if request.method == 'GET':

            searchTerm = request.GET.get("searchTerm","" )    
            sortColumn = request.GET.get("sortColumn","" )
            sortDirection = request.GET.get("sortDirection","" )
            filterString = request.GET.get("filterString","" )            
            sortLymeEvidence = request.GET.get("sortLymeEvidence","" )
            lymeEvidenceFilter = request.GET.get("lymeEvidenceFilter","" )    
            pageButton = request.GET.get("pageButton","" )   
            pageNumValue = request.GET.get("pageNum",0 )  
            downloadFlag = request.GET.get("downloadFlag","" )        

        elif request.method == 'POST':
        
            searchTerm = request.POST.get("searchTerm","" )
    
            sortColumn = request.POST.get("sortColumn","" )
            sortDirection = request.POST.get("sortDirection","" )
            filterString = request.POST.get("filterString","" )
            
            sortLymeEvidence = request.POST.get("sortLymeEvidence","" )
            lymeEvidenceFilter = request.POST.get("lymeEvidenceFilter","" )    
            pageButton = request.POST.get("pageButton","" )   
            pageNumValue = request.POST.get("pageNum",0 )  
            downloadFlag = request.POST.get("downloadFlag","" )        
            
        filterQueryString = ''
        filterList = []
        
        filterMap = {}
        
        firstPage = True
        lastPage = False
        
        lymeFilterMap = {"v1_p__isnull":False, "v2_p__isnull":False, "v5_p__isnull":False}
        
        if filterString != '':
            filterList = filterString.split(",")
            
            filterMap = {str(key) + "__isnull" : False for key in filterList}
        
        print ( " sort direction 000 = " + str(sortDirection) ) 

        if sortLymeEvidence == '' and pageButton == '':
            #print ( " in lyme evidence sort ")
            if sortDirection == '':
                sortDirection ="1"
            else:
                if sortDirection == "1":
                    print ( " in 1")
                    sortDirection = "-1"
                elif sortDirection == "-1":
                    print ( " in -1")
                    sortDirection = "1"
        #print ( " sort direction --- = " + str(sortDirection) ) 
            
        sortDirectionString = 'asc';
        sortColumnString = sortColumn
        if sortDirection == "-1" or sortColumn in ['gene_nlp_p', 'disease_nlp_p', 'chemical_nlp_p', 'sea_p', 'drugbank_p']:
            sortDirectionString = 'desc';
            sortColumnString = "-"+sortColumnString      

        if len(filterMap) == 0 and sortColumn == '':
            omniRecords = LymeOmniIndex.objects.all().order_by("meta_p")
            sortColumn = "meta_p"
            sortDirection = "1"
        else:
            if len(filterMap) != 0:
                omniRecords = LymeOmniIndex.objects.filter(Q(**filterMap))
                if sortColumn != '':
                    omniRecords = omniRecords.order_by(sortColumnString)

            else:
                if sortColumn != '':
                    
                    #print (" sortColumn ** = " + sortColumnString)
                    omniRecords = LymeOmniIndex.objects.all().order_by(sortColumnString)

        if lymeEvidenceFilter == "1" or sortLymeEvidence == "1":
            #print ( " filtering lyme ")
            omniRecords = omniRecords.filter(Q(**lymeFilterMap))
        
        if searchTerm != '':
            omniRecords = omniRecords.filter(gene_symbol__contains = searchTerm)
                    
        totalRecCount = len(omniRecords)
        
        if downloadFlag == "1":
            
            response = HttpResponse(content_type='text/csv')
            
            filename = "evidence_matrix_" + str(datetime.now()).replace("-", "")[0:8] + ".csv"
            response["Content-Disposition"] = "attachment; filename=" + filename
        
            writer = csv.writer(response)
            writer.writerow(['Entrez Gene Id','Gene Symbol','SEA','Drugbank','Meta','Chemical NLP','Disease NLP','Gene NLP','eQTLs Skin','eQTLs LCL','eQTLs Fat','Proteomics','Combined P: Meta & Skin & Proteomic','Combined P: Meta & Skin','Combined P: Meta Skin | Proteomic'])
            
            for rec in omniRecords:
                
                writer.writerow([rec.entrez_gene_id, rec.gene_symbol, rec.sea_p, rec.drugbank_p, rec.meta_p, rec.chemical_nlp_p, rec.disease_nlp_p, rec.gene_nlp_p,rec.skin_p, rec.lcl_p, rec.fat_p, rec.proteomics_p, rec.combined_p_meta_skin_prot, rec.combined_p_meta_skin, rec.combined_p_meta_skin_prot_wildcard])
                print ( " *** ")
            
            response["Content-type"] = "text/csv"
            return response          
        
        fromRec = 0
        toRec = 10

        if pageButton == '' :
            if len(omniRecords) > 10:
                omniRecords = omniRecords[:10]
        else:
            if pageButton == '1':
        
                fromRec = (pageNum+1) * 10
                toRec = fromRec +10

                firstPage = False

                if len(omniRecords) > toRec: 
                    omniRecords = omniRecords[fromRec:toRec]
                else:
                    if fromRec < 10:
                        firstPage = True
                    omniRecords = omniRecords[fromRec:]
                    lastPage = True
                    
                pageNum = pageNum +1
            elif pageButton == '0':
        
                fromRec = (pageNum - 1) * 10
                toRec = pageNum * 10

                if fromRec > 0: 
                    omniRecords = omniRecords[fromRec:toRec]
                    lastPage = False
                    firstPage = False
                    if fromRec < 10:
                        firstPage = True
                else:
                    if toRec > len(omniRecords):
                        toRec = len(omniRecords)
                    omniRecords = omniRecords[:toRec]
                    firstPage = True
                pageNum = pageNum -1
            
        geneRecords = []
            
        for omniRecord in omniRecords:
            
            geneRecord = GeneDetailObj()

            geneRecord.entrezId = omniRecord.entrez_gene_id
            geneRecord.geneSymbol = omniRecord.gene_symbol

            if omniRecord.drugbank_p and omniRecord.drugbank_p != 0:
                geneRecord.drugBankLink = True

            if omniRecord.meta_p and omniRecord.meta_p != 0:
                geneRecord.metaP = '{:0.4e}'.format(omniRecord.meta_p)                
                geneRecord.metaPLink = True

            if omniRecord.fat_p and omniRecord.fat_p != 0:
                geneRecord.fatP = '{:0.4e}'.format(omniRecord.fat_p)                
                geneRecord.fatPLink = True
    
            if omniRecord.skin_p and omniRecord.skin_p != 0:
                geneRecord.skinP = '{:0.4e}'.format(omniRecord.skin_p)                
                geneRecord.skinPLink = True    
        
            if omniRecord.lcl_p and omniRecord.lcl_p != 0:
                geneRecord.lclP = '{:0.4e}'.format(omniRecord.lcl_p)               
                geneRecord.lclPLink = True    
            
            if omniRecord.proteomics_p and omniRecord.proteomics_p and omniRecord.proteomics_p != 0:
                geneRecord.proteomics = '{:0.4e}'.format(omniRecord.proteomics_p)                
                geneRecord.proteomicsLink = True    

            if omniRecord.v1_p and omniRecord.v1_p != 0:
                geneRecord.v1Flag = True                

            if omniRecord.v2_p and omniRecord.v2_p != 0:
                geneRecord.v2Flag = True                
    
            if omniRecord.v5_p and omniRecord.v5_p != 0:
                geneRecord.v5Flag = True  
                
            if geneRecord.v1Flag or geneRecord.v2Flag or geneRecord.v5Flag:
                geneRecord.evidenceLink = True
    
            if omniRecord.chemical_nlp_p and omniRecord.chemical_nlp_p != 0:
                geneRecord.chemicalNLPLink = True                
        
            if omniRecord.disease_nlp_p and omniRecord.disease_nlp_p != 0:
                geneRecord.diseaseNLPLink = True                
            
            if omniRecord.gene_nlp_p and omniRecord.gene_nlp_p != 0:
                geneRecord.geneNLPLink = True                
                
            if omniRecord.sea_p and omniRecord.sea_p != 0:
                geneRecord.seaLink = True        
                
            if omniRecord.combined_p_meta_skin_prot and omniRecord.combined_p_meta_skin_prot != 0:
                geneRecord.combinedPMetaSkinProteomic = '{:0.4e}'.format(omniRecord.combined_p_meta_skin_prot)
                
            if omniRecord.combined_p_meta_skin and omniRecord.combined_p_meta_skin != 0:
                geneRecord.combinedPMetaSkin = '{:0.4e}'.format(omniRecord.combined_p_meta_skin)

            if omniRecord.combined_p_meta_skin_prot_wildcard and omniRecord.combined_p_meta_skin_prot_wildcard != 0:
                geneRecord.combinedPMetaSkinProteomicGt2 = '{:0.4e}'.format(omniRecord.combined_p_meta_skin_prot_wildcard)
                
            geneRecords.append(geneRecord)

        if toRec > totalRecCount:
            toRec = totalRecCount
        showingRecCount = 10
        if totalRecCount < 10:
            showingRecCount = totalRecCount   
            
        #print ( " final sort direction = " + str(sortDirection))
        
    except:
        traceback.print_exc(file=sys.stdout)
    return render(request, 'lymePortal/evidenceMatrixHome.html', {
        "geneRecords":geneRecords,
        "searchTerm":searchTerm,
        "sortColumn":sortColumn,
        "sortDirection":sortDirection,
        "filterString":filterString, 
        "filterList":filterList,
        "totalRecCount":totalRecCount,
        "showingRecCount":showingRecCount,
        "sortLymeEvidence": sortLymeEvidence,
        "lymeEvidenceFilter": lymeEvidenceFilter, 
        "pageNum":pageNum,
        "firstPage":firstPage,
        "lastPage":lastPage,
        "fromRec":fromRec,
        "toRec":toRec,

    }, RequestContext(request))

def submittedJobs(request):
    try:
        print ( " in submittted jobs ")
    except:
      traceback.print_exc(file=sys.stdout)
    return

#def uploadData(request):
    #try:
        #print ( " in upload data ")
    #except:
      #traceback.print_exc(file=sys.stdout)
    #return

#def showEvidenceMatrix(request):
    #try:
        #eegData = loadmat('EEG181.mat')
    #except:
      #traceback.print_exc(file=sys.stdout)
    #return

#def showPubMedData(request):
    #try:
        #eegData = loadmat('EEG181.mat')
    #except:
      #traceback.print_exc(file=sys.stdout)
    #return

#def showPubMedDetails(request):
    #try:
        #eegData = loadmat('EEG181.mat')
    #except:
      #traceback.print_exc(file=sys.stdout)
    #return

@login_required
def listProjects(request):

    projectObjList = []
    try:
        projects = Project.objects.all()
    except:
        traceback.print_exc(file=sys.stdout)    

    return render(request, 'lymePortal/listProjects.html', {
        "projects":projects,
    }, RequestContext(request))

@login_required
def listDatafiles(request):

    datafileList = []
    try:
        
        downloadFlag = request.POST.get("downloadFlag","" )
        
        datafileMap = {settings.DATA_FOLDER + "gene_counts.tsv":"counts_matrix"}        
        datafileObjList = []
        
        for filePath, fileName in datafileMap.items():
            datafileObj = DatafileObj()
            datafileObj.datafileName = fileName
            
            print ( " filePath = " + str(filePath))
            
            df = pd.read_table(filePath,sep='\t', index_col=None)
            
            originalDf = df
              
            if len(df) > 50:
                df = df[:50]
            df = df[df.columns[:10]]
                
            datafileObj.columns= df.columns            
            datafileObj.dataMatrix = df.as_matrix()
            
            datafileObjList.append(datafileObj)
            
            if downloadFlag == "1":
                
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = "attachment; filename='raw_data_"+ str(datetime.now()).replace("-", "")[0:8] + ".csv'"
            
                writer = csv.writer(response)
                writer.writerow(originalDf.columns)
                
                for index, rec in originalDf.iterrows():
                    writer.writerow(list(rec))
                
                response["Content-type"] = "text/csv"
                return response                

    except:
        traceback.print_exc(file=sys.stdout)    

    return render(request, 'lymePortal/listDatafiles.html', {
        "datafileObjList":datafileObjList,
    }, RequestContext(request))

@login_required
def listSequenceProjects(request):

    #projectObjList = []
    try:
        print("**")
        #projects = Project.objects.all()
    except:
        traceback.print_exc(file=sys.stdout)    

    return render(request, 'lymePortal/listSequenceProjects.html', {
        #"projects":projects,
    }, RequestContext(request))

def displayProjectData(request):

    ''' Function process data landing page submit
    Input: params.request
    Output: Delegates task to corresponding handler
    '''

    ##projectId = request.POST.get("projectId","" )
    
    ##dataFiles = Project.objects.filter(pk = projectId)

    countsMatrixFile = ""

@login_required
def fetchGeoData(request):

    try:
        print ( " in fetch geo" ) 

    except:
        traceback.print_exc(file=sys.stdout)

    return render(request, 'lymePortal/fetchGeoData.html', {
        })

@login_required
def submitFetchGeoData(request):

    try:

        geoId = request.POST.get("geoId","" )

    except:
        traceback.print_exc(file=sys.stdout)

    return render(request, 'lymePortal/submitAddProject.html', {
            "project":project,
        })

def listAngularProjects(request):
    try:

        projectObjList = []

        projects = Project.objects.all()

        listProjects = [{"id":x.id, "name":x.name, "description":x.description} for x in projects]

        # listProjects = list(projects)
        print ( "list projects =  " + str(listProjects))

        projectsJsonString = json.dumps(listProjects)
        print ( "projectsJsonString =  " + str(projectsJsonString))

    except:
        traceback.print_exc(file=sys.stdout)
    return HttpResponse(projectsJsonString, content_type="application/json")

def getProjectByName(request):
    try:

        projectName = request.GET.get("projectName","0" )
        print (" name = " + str(projectName) ) 

        projects = Project.objects.filter(name=projectName)
        
        project = ''
        if len(projects) > 0:
            project = projects[0]
    
            projectMap = {"id":project.id, "name":project.name, "description":project.description} 
    
            projectJsonString = json.dumps(projectMap)

            
        print (project)

    except:
        traceback.print_exc(file=sys.stdout)
    return HttpResponse(projectJsonString, content_type="application/json")
    #return projectsJsonString

@login_required
def addProject(request):

    try:

        projects = Project.objects.filter ( user = request.user)

    except:
        traceback.print_exc(file=sys.stdout)
        #messages.add_message(request, messages.ERROR, 'Error occurred while fetching details for sample detail id' + str(sampleDetailId) )

    return render(request, 'lymePortal/addProject.html', {

        "projects" : projects ,

    })

@login_required
def addDataFile(request):

    try:

        projectId = request.POST.get("projectId",0 )

        project = Project.objects.get(pk = projectId)

    except:
        traceback.print_exc(file=sys.stdout)

    return render(request,'rnaseq/addDataFile.html', {

        "project" : project,

    })

@login_required
def addDataFileType(request):

    try:

        print (" starting = " )

    except:
        traceback.print_exc(file=sys.stdout)

    return render(request,'rnaseq/addDataFileType.html', {

    })

@login_required
def submitAddDataFileType(request):

    project = ""

    try:

        fileTypeName = FileType.objects.get(pk= fileTypeId)

        filePath = settings.DATA_OUTPUT_FOLDER + "/Project_" + str(projectId)

        if not os.path.isdir( filePath ):
            os.mkdir(filePath)

        # counts matrix
        dataFile = request.FILES["dataFile"]

        data = [row for row in csv.reader(dataFile.read().decode().split("\n"), dialect=csv.excel_tab)]

        outputFile = open(filePath + "/" + str(dataFileName) +".csv", "w")

        for i, line in enumerate(data):

            if len(line) > 0:

                outputFile.write(",".join(line))
                outputFile.write("\n")

        dataFile = DataFile ( name = dataFileName , description = dataFileDescription, project = project, filePath = filePath, fileType = fileType)

        dataFile.save()
        
        dataFiles = DataFile.objects.all()

    except:
        traceback.print_exc(file=sys.stdout)
        messages.add_message(request, messages.ERROR, 'Error occurred while fetching details for data file id' + str(dataFileId) )

    return render(request,'rnaseq/listFiles.html', {
        "project":project,
        "dataFiles":dataFiles,
    })

@login_required
def submitAddDataFile(request):

    project = ""

    try:

        projectId = request.POST.get("projectId",0 )
        project = Project.objects.get (pk = projectId)

        fileTypeId = request.POST.get("projectId",0 )
        fileType = FileType.objects.get(pk= fileTypeId)

        filePath = settings.DATA_OUTPUT_FOLDER + "/Project_" + str(projectId)

        if not os.path.isdir( filePath ):
            os.mkdir(filePath)

        # counts matrix
        dataFile = request.FILES["dataFile"]

        data = [row for row in csv.reader(dataFile.read().decode().split("\n"), dialect=csv.excel_tab)]

        outputFile = open(filePath + "/" + str(dataFileName) +".csv", "w")

        for i, line in enumerate(data):

            if len(line) > 0:

                outputFile.write(",".join(line))
                outputFile.write("\n")

        dataFile = DataFile ( name = dataFileName , description = dataFileDescription, project = project, filePath = filePath, fileType = fileType)

        dataFile.save()
        
        dataFiles = DataFile.objects.all()

    except:
        traceback.print_exc(file=sys.stdout)
        messages.add_message(request, messages.ERROR, 'Error occurred while fetching details for data file id' + str(dataFileId) )

    return render(request,'rnaseq/listFiles.html', {
        "project":project,
        "dataFiles":dataFiles,
    })

@login_required
def submitAddProject(request):

    messages = []

    try:

        projectName = request.POST.get("projectName","" )
        projectDescription = request.POST.get("projectDescription","" )

        project = Project ( name = projectName, description = projectDescription )
        project.save()

    except:
        traceback.print_exc(file=sys.stdout)

    return render(request, 'lymePortal/submitAddProject.html', {
            "project":project,
        })

@login_required
def listDatabases(request):

    try:

        databases = [] 
        #databases = sorted(list(set(df1["db"].tolist())))
        fileNames = [settings.DATA_FOLDER + "gsea_results_v1.tsv",settings.DATA_FOLDER + "gsea_results_v2.tsv" ,settings.DATA_FOLDER + "gsea_results_v5.tsv" ]

        fileNameMap = {settings.DATA_FOLDER + "gsea_results_v1.tsv":"gsea_v1",settings.DATA_FOLDER + "gsea_results_v2.tsv":"gsea_v2" ,settings.DATA_FOLDER + "gsea_results_v5.tsv":"gsea_v5" }
        
        dfs = [pd.read_table(fileName,sep='\t', index_col=None) for fileName in fileNames]
        
        #datafileMap = {settings.DATA_FOLDER + "gene_counts.tsv":"counts_matrix"}        
        datafileObjList = []
        
        downloadFlag = request.POST.get("downloadFlag","" )
        databaseIndex = 0
        try:
            databaseIndex = int(request.POST.get("databaseIndex","0" ))
        except:
            pass
        
        for index, df in enumerate(dfs):
            
            if downloadFlag == "1" and databaseIndex == index :
                
                response = HttpResponse(content_type='text/csv')
                
                filename = "gsea_" + fileNameMap[fileNames[index]] + "_" + str(datetime.now()).replace("-", "")[0:8] + ".csv"
                response["Content-Disposition"] = "attachment; filename=" + filename
            
                writer = csv.writer(response)
                writer.writerow(df.columns)
                
                for index1, rec in df.iterrows():
                    writer.writerow(list(rec))
                
                response["Content-type"] = "text/csv"
                return response            
            
            if len(df) > 50:
                df = df[:50]

            datafileObj = DatafileObj()

            datafileObj.datafileName = fileNameMap[fileNames[index]]
            datafileObj.columns= df.columns            
            datafileObj.dataMatrix = df.as_matrix()
            
            datafileObjList.append(datafileObj)
        
        dbSet = set()
        
        for df in dfs:
            print (df[".id"].tolist())
            dbSet = dbSet.union(set(df[".id"].tolist()))
        
        databases = sorted(list(dbSet))
        
        print (databases)

    except:
        traceback.print_exc(file=sys.stdout)

    return render(request, 'lymePortal/listDatabases.html', {
            "databases":databases,
            "datafileObjList":datafileObjList,
            "fileNames":fileNames,
        })

@login_required
def visualizationOptions(request):

    messages = []

    try:
        
        databases = request.POST.getlist("database" )
        print (" databases " + str(databases))
        
        treemapDatabase = request.POST.get("treemapDatabase" ,"")
        reloadPage = request.POST.get("reloadPage" , "0")
        
        fileNames = [settings.DATA_FOLDER + "gsea_results_v1.tsv",settings.DATA_FOLDER + "gsea_results_v2.tsv" ,settings.DATA_FOLDER + "gsea_results_v5.tsv" ]
        
        fileNameMap = {settings.DATA_FOLDER + "gsea_results_v1.tsv":"Before_Antibiotics",settings.DATA_FOLDER + "gsea_results_v2.tsv":"After_Antibiotics",settings.DATA_FOLDER + "gsea_results_v5.tsv":"6_months_post_treatment"}
        directions = ["up", "down", "all"]
        
        dfs = [pd.read_table(fileName,sep='\t', index_col=None) for fileName in fileNames]
        
        dbFileMap = {}
        dbPathwayMap = {}
        
        adjacencyMatrix = []
        
        for indexDf, df in enumerate(dfs):
            
            fileName = fileNames[indexDf]
            
            for indexData, row in df.iterrows():
                
                #print (" index for file " + str(fileName) + " value = " + str(indexData) )
                
                database = row[".id"]
                
                if database not in dbPathwayMap:
                    #print (" *** new db " + str(database)) 
                    dbPathwayMap[database] = {}
                    
                if database not in dbFileMap:
                    dbFileMap[database] = {}
                    
                if fileName not in dbPathwayMap[database]:
                    #print (" *** new fileName " + str(fileName)) 
                    dbPathwayMap[database][fileName] = {}

                if fileName not in dbFileMap:
                    dbFileMap[database][fileName] = 0
                    
                pathway = row["module"] + "-" + row["feature"]
                
                adjPValue = float(row["Adjusted.Pvalue"])
                geneList = row["enrichment_overlap"].split("|")
                #print ( " splitting genes " + str(geneList) )
                
                #if pathway not in dbPathwayMap[database][fileName]:
                dbPathwayMap[database][fileName][pathway] = (adjPValue, geneList, row["module"], row['Gene.Set.Size'], row['Total.Hits'], row['Expected.Hits'], row['Observed.Hits']) 

                dbFileMap[database][fileName] += -1 * np.log10(adjPValue)
                
        #print (dbPathwayMap ) 
                
        treeMapDataList = []
        
        geneSymbolDF = pd.read_table("genelistmapping.txt",sep='\t', index_col=None)
        geneSymbolDF["entrezID"] = geneSymbolDF["entrezID"].astype(int)        
        entrezIDList = geneSymbolDF["entrezID"].tolist()
        geneSymbolList = geneSymbolDF["geneSymbol"].tolist()
        print (entrezIDList[:20])
        print (geneSymbolList[:20])
        
        dataRows = []

        if len(databases) > 0:
            
            if reloadPage == "1" and treemapDatabase != '':
                database = treemapDatabase
            else:
                database = databases[0]
                treemapDatabase = database                
            treeMapDataList.append([database, "null", 0])
            
            #print (dbFileMap[database])

            if database in dbFileMap:
                for fileName, sumAdjPVal in dbFileMap[database].items():
                    treeMapDataList.append([fileName, database, sumAdjPVal])

                    if database in dbPathwayMap:
                        #print ( " database " + str(database) + " :: " + str(dbPathwayMap[database]) )
                        if fileName in dbPathwayMap[database]:
                            #print ( dbPathwayMap[database][fileName] )      
                            fileNamePrefix = fileName
                            if len(fileNamePrefix) > 5:
                                fileNamePrefix = fileNamePrefix[-6:-4]
                            #print(dbPathwayMap[database][fileName])
                            for pathway, tupleValue in dbPathwayMap[database][fileName].items():
                                treeMapDataList.append([ pathway + "(" + fileNamePrefix + ")" , fileName, -1 * np.log10(tupleValue[0] ) ] )
                                
                                adjPValue = tupleValue[0]
                                geneList = tupleValue[1]
                                pathwayName = tupleValue[2]
                                
                                #row['Gene.Set.Size'], row['Total.Hits'], row['Expected.Hits'], row['Observed.Hits']       
                                
                                geneSetSize = tupleValue[3]
                                totalHits = tupleValue[4]
                                expectedHits = tupleValue[5]
                                observedHits = tupleValue[6]
                                
                                #geneString = ["<a href='https://www.ncbi.nlm.nih.gov/gene/{{x}}'>{{x}}</a>" for x in geneList])
                                #print (pathway + "(" + fileNamePrefix + "): " + str(geneList))
                                geneSymbolList1 = [geneSymbolList [ entrezIDList.index(int(x))] for x in geneList]
                                geneTupleList = [(x,y) for x,y in zip(geneList, geneSymbolList1)] 
                                dataRows.append([fileNameMap[fileName], pathway, geneSetSize, totalHits, '{:0.4e}'.format(expectedHits), observedHits, '{:0.4e}'.format(adjPValue), geneTupleList, pathwayName])
                                for gene in geneList:
                                    geneName = gene
                                    
                                    if int(gene) in entrezIDList:
                                        
                                        #print (" found gene " + str(gene) ) 
                                        geneName = geneSymbolList [ entrezIDList.index(int(gene))]
                                        link = "<a href = 'https://www.ncbi.nlm.nih.gov/gene/?term=" + gene + "'>"
                                        
                                        #print (" found gene 2" + str(geneName) ) 
                                    treeMapDataList.append([   geneName + "(" + fileNamePrefix + ":" + pathway + ")", pathway + "(" + fileNamePrefix + ")", 1])
                        
        print ( str(type(dataRows)) ) 
    except:
        traceback.print_exc(file=sys.stdout)
        
    return render(request, 'lymePortal/visualizationOptions.html', {
            "databases":databases,
            "directions":directions,
            "fileNames":fileNames,
            "treeMapDataList":treeMapDataList,
            "treemapDatabase":treemapDatabase,
            "dataRows":dataRows,
        })

@login_required
def visualizeData(request):

    messages = []

    try:

        database = request.POST.get("database","" )
        direction = request.POST.get("direction","" )
        fileName1 = request.POST.get("fileName1","" )
        fileName2 = request.POST.get("fileName2","" )
        
        fileNameMap = {settings.DATA_FOLDER + "gsea_results_v1.tsv":"Before_Antibiotics",settings.DATA_FOLDER + "gsea_results_v2.tsv":"After_Antibiotics",settings.DATA_FOLDER + "gsea_results_v5.tsv":"6_months_post_treatment"}
        
        fileName1Display = fileNameMap[fileName1]
        fileName2Display = fileNameMap[fileName2]        
        
        #print (" fileName1 " + str(fileName1) + " fileName2 " + str(fileName2) + " database " + str(database) + " direction " + str(direction) ) 
        
        mergedDF = createMergedDF(fileName1, fileName2, database, direction)        
        
        #print ( mergedDF.head()) 
              
        #mergedDF["pathway"] = mergedDF["pathway"].str[:6]
        mergedDF["pathway"] = mergedDF["pathway"]

        geneSymbolDF = pd.read_table("genelistmapping.txt",sep='\t', index_col=None)
        #print ( geneSymbolDF.head() ) 
        geneSymbolDF = geneSymbolDF.rename(columns={"entrezID":"gene"})
        #print ( geneSymbolDF.head() )
        geneSymbolDF["gene"] = geneSymbolDF["gene"].astype(int)
        
        mergedDF["gene"] = mergedDF["gene"].astype(int)

        mergedDF = mergedDF.merge (geneSymbolDF , on = "gene") 

        #mergedDF["gene"] = mergedDF["geneSymbol"]

        mergedDF["colors"] = list(  sns.color_palette("hls", len(mergedDF)).as_hex()  )

        print ( mergedDF.head() )         

        dfMatrix = mergedDF.as_matrix()
        
        #print(dfMatrix)

    except:
        traceback.print_exc(file=sys.stdout)

    return render(request, 'lymePortal/visualizeData.html', {
            "dfMatrix":dfMatrix,
            "database":database,
            "fileName1Display":fileName1Display,
            "fileName2Display":fileName2Display,        
            
        })

@login_required
def showBoxPlots(request):

    messages = []

    try:

        entrezIdString = request.POST.get("entrezIds","" )
        entrezIds = entrezIdString.split(",")[:-1]
        print (entrezIds)

        geneSymbolDF = pd.read_table("genelistmapping.txt",sep='\t', index_col=None)
        geneSymbolDF["entrezID"] = geneSymbolDF["entrezID"].astype(int)        
        entrezIDList = geneSymbolDF["entrezID"].tolist()
        geneSymbolList = geneSymbolDF["geneSymbol"].tolist()
        
        genes = [geneSymbolList [ entrezIDList.index(int(x))] for x in entrezIds]

        geneImageMap = {}

        pd.options.mode.chained_assignment = None
        
        countsFile = settings.DATA_FOLDER + "gene_counts.tsv"
        df = pd.read_table(countsFile,sep='\t', index_col=None)
        
        for gene in genes:
            df1 = df[df['Geneid'] == gene]
            df1.iloc[0].tolist()[1:]
            
            dfnew = pd.DataFrame(index=None, columns=['Expression', 'SampleId','Status', 'Visit'])
            dfnew['Expression'] = df1.iloc[0].tolist()[1:]
            dfnew['SampleId'] = df.columns[1:]
            dfnew = dfnew[(dfnew['SampleId'].str.find('JHU-01')!= -1) | (dfnew['SampleId'].str.find('JHU-02')!= -1)]
            dfnew['Status'] = [ 'Control' if x.find('JHU-02')!= -1 else 'Case' for x in dfnew['SampleId'] ] 
            
            dfnew['Visit'] = [x[-1:] for x in dfnew['SampleId']]
            dfnew['Visit'] = ['1' if dfnew['Status'][i] == 'Control' else x for i,x in enumerate(dfnew['Visit']) ]
            
            dfControl2 = dfnew[dfnew['Status'] == 'Control']
            dfControl5 = dfnew[dfnew['Status'] == 'Control']
            
            dfControl2['Visit'] = '2'
            #dfnew = dfnew.append(dfControl2)
            
            dfControl5['Visit'] = '5'
            #dfnew = dfnew.append(dfControl5)
            dfnew = pd.concat([dfnew, dfControl2, dfControl5], ignore_index=True)
            dfnew = dfnew[dfnew['Visit'].isin(['1','2','5'])]
            plt.clf()
            fig = plt.figure()
            fig.patch.set_facecolor('white')
            snsPlot = sns.boxplot(x="Visit", y="Expression", hue="Status", data=dfnew, palette="PRGn")
            imagePath = settings.IMAGE_FOLDER + "plots/"+ gene + "_boxplot.png"
            plt.savefig(imagePath)
            
            dfnew['Expression'] = [str(x) for x in dfnew['Expression']]
            geneImageMap[gene] = (gene + "_boxplot", dfnew.as_matrix())
            
            #dfnew.to_csv(settings.DATA_FOLDER + "boxplotdata_"+gene+".csv", index=None)
            #print(dfnew.as_matrix())
            
    except:
        traceback.print_exc(file=sys.stdout)

    return render(request, 'lymePortal/showBoxPlots.html', {
            "geneImageMap":geneImageMap,
        })

def displayGeneDetails(request):

    messages = []

    try:

        geneRecords = []
        
        entrezIdString = request.POST.get("entrezIds","" )

        print (entrezIdString)            
        entrezIds = entrezIdString.split(",")[:-1]
        print (entrezIds)

        if len(entrezIds) > 0 and entrezIds[0] != '':
            
            omniRecords = LymeOmni.objects.filter(entrez_gene_id__in = entrezIds)
            geneMap = {}
    
            for omniRecord in omniRecords:
                
                entrezId = omniRecord.entrez_gene_id
                geneSymbol = omniRecord.gene_symbol
                sourceTable = omniRecord.source_table
                val = omniRecord.val
                feature = omniRecord.feature
                recordId = omniRecord.id
                
                fisherScores = LymeOmniFisherScores.objects.filter(entrez_gene_id = entrezId)
                
                if entrezId not in geneMap:
                    geneDetailObj = GeneDetailObj()
                    geneMap[entrezId] = geneDetailObj
                
                geneDetailObj = geneMap[entrezId]
                geneDetailObj.entrezId = entrezId
                geneDetailObj.geneSymbol = geneSymbol
                if sourceTable == 'lyme_omni_drugbank_targets':
                    geneDetailObj.drugBankLink = True
                elif sourceTable == 'lyme_omni_eqtls':
                    if isinstance(val, numbers.Number):
                        val = '{:0.4e}'.format(float(val))
                        #np.round(val,4)
                    if feature == 'fat_p':
                        geneDetailObj.fatP = '{:0.4e}'.format(float(val))
                        geneDetailObj.fatPLink = True
                    elif feature == 'lcl_p':
                        geneDetailObj.lclP = '{:0.4e}'.format(float(val))
                        geneDetailObj.lclPLink = True
                    elif feature == 'skin_p':
                        geneDetailObj.skinP = '{:0.4e}'.format(float(val))
                        geneDetailObj.skinPLink = True
                elif sourceTable == 'lyme_omni_nlp_meta':
                    print ( " in meta " + str(feature)) 
                    if feature == 'chemical':
                        geneDetailObj.chemicalNLPLink = True
                    elif feature == 'disease':
                        geneDetailObj.diseaseNLPLink = True
                    elif feature == 'gene':
                        geneDetailObj.geneNLPLink = True
                elif sourceTable == 'lyme_omni_sea_targets':
                    print ( " in sea " + str(feature)) 
                    geneDetailObj.seaLink = True                    
                    geneDetailObj.feature = 'sea'
                elif sourceTable == 'lyme_omni_meta_lyme_casevsctrl_v2':
                    print ( " in meta " + str(feature)) 
                    geneDetailObj.metaPLink = True                    
                    geneDetailObj.feature = 'meta' 
                    geneDetailObj.metaP = '{:0.4e}'.format(float(val))    
                elif sourceTable == 'lyme_omni_proteomics':
                    print ( " in proteomics " + str(feature)) 
                    geneDetailObj.proteomicsLink = True                    
                    geneDetailObj.feature = 'proteomics' 
                    geneDetailObj.proteomics = '{:0.4e}'.format(float(val))
                for fisherScore in fisherScores:
                    if fisherScore.fisher_type == 1: 
                        geneDetailObj.combinedPMetaSkinProteomic = '{:0.4e}'.format(fisherScore.fisher_score)
                    if fisherScore.fisher_type == 2: 
                        geneDetailObj.combinedPMetaSkin = '{:0.4e}'.format(fisherScore.fisher_score)
                    elif fisherScore.fisher_type == 3:
                        geneDetailObj.combinedPMetaSkinProteomicGt2 = '{:0.4e}'.format(fisherScore.fisher_score)
            geneRecords = geneMap.values()
        pubmedRecordList = LymeOmniNlp.objects.filter(dbid_2_type = 'entrez', dbid_2__in = entrezIds)
        pubmedRecords = []
        for rec in pubmedRecordList:
            pubmedRecords.append([rec.term_1, rec.term_1_type, rec.dbid_1, rec.term_2, rec.term_2_type, rec.dbid_2, rec.relation, '{:0.4e}'.format(rec.adjpvalue), rec.id1, rec.id2])
	
        print ( pubmedRecords )
        
        #print (str(omniRecords))
    except:
        traceback.print_exc(file=sys.stdout)

    return render(request, 'lymePortal/displayGeneDetails.html', {
            "geneRecords":geneRecords,
            "entrezIds":entrezIdString,
            "pubmedRecords":pubmedRecords,   
        })

#@login_required
def fetchPmidDetail(request):
    
    try:
        
        term1 = request.POST.get("term1","" )
        term2 = request.POST.get("term2","" )
        
        termId1 = request.POST.get("termId1","" )
        termId2 = request.POST.get("termId2","" )    
        
        downloadFlag = request.POST.get("downloadFlag","0" )            
        
        print (" termId1 = " + termId1)
        print (" termId2 = " + termId2)

        pmidDataRecs = []
        
        if downloadFlag == "1":
            
            response = HttpResponse(content_type='text/csv')
            filename = "lyme_omni_pubmed_" + str(datetime.now()).replace("-", "")[0:8] + ".csv"
            response["Content-Disposition"] = "attachment; filename=" + filename            
  
            writer = csv.writer(response)

            writer.writerow(['Journal','Title','Submission Date','Impact Factor','Relevance Score','Article Link'])

        cursor = connection.cursor()
        
        print ('select pmid.journal as journal, pmid.title as title, pmid.submission_date as submissionData, pmid.impact_factor as impactFactor, ipm1.pmid as pmid, ipm1.term_id as id1, ipm2.term_id as id2, (ipm1.tf_idf+ipm2.tf_idf) as relevanceScore from lyme.lyme_omni_nlp_id_to_pmid_map ipm1 inner join lyme.lyme_omni_nlp_id_to_pmid_map ipm2 on ipm1.pmid = ipm2.pmid inner join lyme.lyme_omni_nlp_pmid_data pmid on pmid.pmid = ipm1.pmid where ipm1.term_id = ' + termId1 + ' and ipm2.term_id = ' + termId2)
        rows = cursor.execute('select pmid.journal as journal, pmid.title as title, pmid.submission_date as submissionData, pmid.impact_factor as impactFactor, ipm1.pmid as pmid, ipm1.term_id as id1, ipm2.term_id as id2, (ipm1.tf_idf+ipm2.tf_idf) as relevanceScore from lyme.lyme_omni_nlp_id_to_pmid_map ipm1 inner join lyme.lyme_omni_nlp_id_to_pmid_map ipm2 on ipm1.pmid = ipm2.pmid inner join lyme.lyme_omni_nlp_pmid_data pmid on pmid.pmid = ipm1.pmid where ipm1.term_id = ' + termId1 + ' and ipm2.term_id = ' + termId2)

        while True:
            row = cursor.fetchone()
            if row is None:
                break                    
            #print (row)
            
            if downloadFlag == "1":
                
                writer.writerow([row[0], row[1], row[2], row[3] ,row[7],'http://www.ncbi.nlm.nih.gov/pubmed/%s' % (row[4])])
                print ( " *** ")
            
            else:

                impactFactor = 0
                if row[3] and isinstance(row[3],numbers.Number):
                    impactFactor = round(row[3], 2)
    
                pmidDataRecs.append([row[0], row[1], row[2], impactFactor ,round(row[7], 2),'http://www.ncbi.nlm.nih.gov/pubmed/%s' % (row[4])])   
    
                if len(pmidDataRecs)> 500:
                    break
            
        if downloadFlag == "1":

            response["Content-type"] = "text/csv"
            return response              
            
    except:
        traceback.print_exc(file=sys.stdout)

    return render(request, 'lymePortal/displayPmidDetail.html', {
            "pmidDataRecs":pmidDataRecs,
            "term1":term1,
            "term2":term2,
            "termId1":termId1,
            "termId2":termId2,
        })

#@login_required
def fetchOmniDetail(request):
    
    try:
        
        downloadFlag = "0"
        
        if request.method == 'GET':
            entrezId = request.GET.get("entrezId","" )
            feature = request.GET.get("feature","" )
            entrezIds = [entrezId]
            searchTerm = ''
        elif request.method == 'POST':
        
            searchTerm = request.POST.get("searchTerm","" )       
            
            entrezId = request.POST.get("entrezId","" )
            feature = request.POST.get("feature","" )
            
            print ("entrezId = " + str(entrezId))
            print ("feature = " + str(feature))
    
            entrezIds = request.POST.get("entrezIds","" )
            downloadFlag = request.POST.get("downloadFlag","0" )
	    
        permanentLink = HOST_NAME + "lymePortal/fetchOmniDetail/?entrezId="+str(entrezId)+"&feature="+str(feature)	    
        
        geneSymbolDF = pd.read_table("genelistmapping.txt",sep='\t', index_col=None)
        geneSymbolDF["entrezID"] = geneSymbolDF["entrezID"].astype(int)        
        entrezIDList = geneSymbolDF["entrezID"].tolist()
        geneSymbolList = geneSymbolDF["geneSymbol"].tolist()
        
        gene = geneSymbolList [ entrezIDList.index(int(entrezId))]
        
        introHeader = ''
        introText = ''
        
        if downloadFlag == "1":
            
            response = HttpResponse(content_type='text/csv')
            
            filename = "lyme_omni_nlp_" + feature + "_" + str(datetime.now()).replace("-", "")[0:8] + ".csv"
            response["Content-Disposition"] = "attachment; filename=" + filename
        
            writer = csv.writer(response)
        
        if feature == 'chemical':
            introHeader = 'NLP Chemical Relationships'
            introText = 'Chemical NLP – statistical association between gene and chemical found from applying natural language processing (NLP) to all abstracts in the scientific literature at PubMed. Empty cells indicate no statistical association found between a gene and another chemical published biomedical abstracts. Clicking on a checkbox opens a popup window that shows the list of chemicals (Term 2) and their association to the specific gene (Term 1).'   
        elif feature == 'disease':
            introHeader = 'NLP Disease Relationships'
            introText = 'Disease NLP – statistical association between gene and other diseases (Lyme + Non-Lyme) found from applying natural language processing (NLP) to all abstracts in the scientific literature at PubMed. Empty cells indicate no statistical association found a gene and another disease from published biomedical abstracts. Clicking on a checkbox opens a popup window that shows the list of diseases (Term 2) and their association to the specific gene (Term 1)'   
        elif feature == 'gene':
            introHeader = 'NLP Gene Relationships'
            introText = 'Gene NLP – statistical association between the gene pairs found from applying natural language processing (NLP) to all abstracts in the scientific literature at PubMed. Empty cells indicate no statistical association found between a gene and another gene from searching published biomedical abstracts. Clicking on a checkbox opens a popup window that shows the list of genes (Term 2) and their association to the specific gene (Term 1).'    
        elif feature == 'drugbank':
            introHeader = 'Drugbank Targets'
            introText = 'Drug Bank – statistical association between a gene product (e.g., the protein) and a drug reported in DrugBank database https://www.drugbank.ca. These connections are based on reported molecular interactions from the biomedical literature. Empty cells indicate no reported association found between a gene and a drug in the drugbank database. Clicking on the checkbox opens a popup window that shows the list of drugs (Chemical Name) that are reported to target, and potentially influence, the protein of the specific gene.'   
        elif feature == 'sea':
            introHeader = 'SEA Targets'
            introText = 'SEA – statistical association between gene and chemical found through matching chemical and gene products using structural similarity (Similarity Ensemble Analysis, PMID: https://www.ncbi.nlm.nih.gov/pubmed/17287757). Empty cells indicate no statistical association found between a gene and another chemical using structural similarity matching. Clicking on the checkbox opens a popup window that shows the list of chemicals (Chemical Name) that are predicted to target, and potentially influence, the protein of the specific gene.'   
        elif feature == 'meta':
            introHeader = 'Meta'
            introText = 'Meta P – p-value from the meta-analysis that combines evidence from multiple differential gene expression studies. Clicking on the numerical value opens a popup window that shows the relevant parameters of the meta-analysis (effect size, standard error of the effect size, q-value (false discovery parameter), number of samples and studies examined).'   
        elif feature == 'proteomics':
            introHeader = 'Proteomics'
            introText = 'Proteomics – p-value from studies that examined differential protein levels in cases and controls. Empty cells mean no data available on a specific protein. Clicking on the numerical value opens a popup window that shows more information on the protein and the relevant parameters from the analysis (tissue type, groups, p-value, and mean difference in measured expression).'  
        elif feature in ['skin_p', 'fat_p', 'lcl_p']:
            introHeader = 'eQTLs'
            introText = 'eQTLs Skin P – p-value of expression quantitative trait loci (eQTLs) found through statistical analysis of skin samples. This analysis examines the association between genetic variation (SNPs) and differential expression values, which indicate potential gene drivers for Lyme in skin tissue. Clicking on the numerical value opens a popup window that shows the relevant parameters from the analysis (Chromosome location, SNP ID, tissue type, p-value, beta-value direction and magnitude of association, standard error, start and stop location on chromosome, allele, and frequency).<br/>LCL P – p-value of expression quantitative trait loci (eQTLs) found through statistical analysis of lymphoblast cell lines. This analysis examines the association between genetic variation (SNPs) and differential expression values, which indicate potential gene drivers for Lyme in lymphoid cells. Clicking on the numerical value opens a popup window that shows the relevant parameters from the analysis (Chromosome location, SNP ID, tissue type, p-value, beta-value direction and magnitude of association, standard error, start and stop location on chromosome, allele, and frequency).<br/>Fat P – p-value of expression quantitative trait loci (eQTLs) found through statistical analysis of fat samples. This analysis examines the association between genetic variation (SNPs) and differential expression values, which indicate potential gene drivers for Lyme in fat tissue. Clicking on the numerical value opens a popup window that shows the relevant parameters from the analysis (Chromosome location, SNP ID, tissue type, p-value, beta-value direction and magnitude of association, standard error, start and stop location on chromosome, allele, and frequency).'   
            
        nlpDetailRecords = []
            
        if feature in ['chemical', 'disease', 'gene']:
            nlpDetailRecordList = LymeOmniNlpMeta.objects.filter(dbid_1_type__iexact = 'entrez', dbid_1 = entrezId, term_2_type__iexact = feature)
            
            if downloadFlag == "1":

                writer.writerow(['Dbid_1','Dbid_1 Type','Term1','Dbid_2','Dbid_2 Type','Term2','Adjusted P Value', 'Co-ocurrence_count', 'Relation', 'P Value', 'Species'])

            for rec in nlpDetailRecordList:
                if downloadFlag == "1":
                    writer.writerow([rec.dbid_1, rec.dbid_1_type, rec.term_1, rec.dbid_2, rec.dbid_2_type, rec.term_2, '{:0.4e}'.format(rec.adjpvalue), rec.coocurrence_count, rec.relation, '{:0.4e}'.format(rec.pvalue), rec.dbid_1_species])
                else:
                    nlpDetailRecords.append([rec.dbid_1, rec.dbid_1_type, rec.term_1, rec.dbid_2, rec.dbid_2_type, rec.term_2, '{:0.4e}'.format(rec.adjpvalue), rec.coocurrence_count, rec.relation, '{:0.4e}'.format(rec.pvalue), rec.dbid_1_species])

        elif feature == 'drugbank':
                nlpDetailRecordList = LymeOmniDrugbankTargets.objects.filter(entrez_gene_id = entrezId)

                if downloadFlag == "1":
    
                    writer.writerow(['Drugbank ID','Chemical Name','Type','UniProt ID','UniProt Name','Entrez ID','Gene Symbol', 'Organism'])

                for rec in nlpDetailRecordList:
                    
                    if downloadFlag == "1":
                    
                        writer.writerow([rec.drug_bank_id, rec.name, rec.type, rec.uni_prot_id, rec.uni_prot_name, rec.entrez_gene_id, rec.gene_symbol, rec.organism])
                    
                    else:
                    
                        nlpDetailRecords.append([rec.drug_bank_id, rec.name, rec.type, rec.uni_prot_id, rec.uni_prot_name, rec.entrez_gene_id, rec.gene_symbol, rec.organism])  

        elif feature == 'sea':
	    
                seaRecordList = LymeOmniSeaTargets.objects.filter(entrez_gene_id = entrezId)

                if downloadFlag == "1":
	
                    writer.writerow(['Chemical Name','Entrez ID','Gene Symbol','Organims'])
                    
                for rec in seaRecordList:
                    
                    if downloadFlag == "1":
                    
                        writer.writerow([rec.name, rec.entrez_gene_id, rec.gene_symbol, rec.organism])
                    
                    else:
                    
                        nlpDetailRecords.append([rec.name, rec.entrez_gene_id, rec.gene_symbol, rec.organism])  

        elif feature == 'meta':
	    
                metaRecordList = LymeOmniMetaLymeCasevsctrlV2.objects.filter(entrez_gene_id = entrezId)
                
                if downloadFlag == "1":
                    
                    writer.writerow(['Entrez ID','Gene Symbol','Tissue','Groups', 'Meta Effect Size', 'Meta Effect Size SE', 'Meta P', 'Meta Q', 'NSamp1', 'NSamp2', 'NStudies', 'Multi Entrez IDs', 'Multi Gene Symbol'])                
                
                for rec in metaRecordList:
                    
                    if downloadFlag == "1":
                        
                        writer.writerow([rec.entrez_gene_id, rec.gene_symbol, rec.tissue, rec.groups, rec.meta_effect_size, rec.meta_effect_size_se, rec.meta_p, rec.meta_qv, rec.n_samp_1, rec.n_samp_2, rec.n_studies, rec. multi_entrez_gene_ids, rec.multi_gene_symbols])

                    else:

                        nlpDetailRecords.append([rec.entrez_gene_id, rec.gene_symbol, rec.tissue, rec.groups, rec.meta_effect_size, rec.meta_effect_size_se, rec.meta_p, rec.meta_qv, rec.n_samp_1, rec.n_samp_2, rec.n_studies, rec. multi_entrez_gene_ids, rec.multi_gene_symbols])  
                    
        elif feature == 'proteomics':
            
                proteomicsRecordList = LymeOmniProteomics.objects.filter(entrez_gene_id = entrezId)
                
                if downloadFlag == "1":
                    
                    writer.writerow(['Protein Name','UniProt ID','Entrez ID','Tissue', 'Groups', 'P', 'Mean Diff'])                
                
                for rec in proteomicsRecordList:
                    
                    if downloadFlag == "1":
                    
                        writer.writerow([rec.protein_name, rec.uni_prot_id, rec.entrez_gene_id, rec.tissue, rec.grps, rec.p, rec.mean_diff])
                    
                    else:
                    
                        nlpDetailRecords.append([rec.protein_name, rec.uni_prot_id, rec.entrez_gene_id, rec.tissue, rec.grps, rec.p, rec.mean_diff])
                    
        elif feature in ['skin_p', 'fat_p', 'lcl_p']:
	    
                eqtlRecordList = LymeOmniEqtls.objects.filter(entrez_gene_id = entrezId)
                
                if downloadFlag == "1":
                    
                    writer.writerow(['Entrez ID','Gene Symbol','Chr','Probe', 'Snp ID', 'Snp Info', 'Snp Coor', 'Type', 'P', 'Beta', 'SE Beta', 'TSS', 'TSE', 'A1', 'Freq A1'])                
                
                for rec in eqtlRecordList:
                        
                    if downloadFlag == "1":
                        
                        writer.writerow([rec.entrez_gene_id, rec.gene_symbol, rec.chr, rec.probe, rec.snp_id,  'fat', rec.fat_p, rec.fat_beta, rec.fat_sebeta, rec.tss, rec.tse, rec.a1, rec.freq_a1]) 
                        writer.writerow([rec.entrez_gene_id, rec.gene_symbol, rec.chr, rec.probe, rec.snp_id,  'skin', rec.skin_p, rec.skin_beta, rec.skin_se_beta, rec.tss, rec.tse, rec.a1, rec.freq_a1]) 
                        writer.writerow([rec.entrez_gene_id, rec.gene_symbol, rec.chr, rec.probe, rec.snp_id,  'lcl', rec.lcl_p, rec.lcl_beta, rec.lcl_sebeta, rec.tss, rec.tse, rec.a1, rec.freq_a1]) 
                    
                    else:                        

                        nlpDetailRecords.append([rec.entrez_gene_id, rec.gene_symbol, rec.chr, rec.probe, rec.snp_id,  'fat', rec.fat_p, rec.fat_beta, rec.fat_sebeta, rec.tss, rec.tse, rec.a1, rec.freq_a1]) 
                        nlpDetailRecords.append([rec.entrez_gene_id, rec.gene_symbol, rec.chr, rec.probe, rec.snp_id,  'skin', rec.skin_p, rec.skin_beta, rec.skin_se_beta, rec.tss, rec.tse, rec.a1, rec.freq_a1]) 
                        nlpDetailRecords.append([rec.entrez_gene_id, rec.gene_symbol, rec.chr, rec.probe, rec.snp_id,  'lcl', rec.lcl_p, rec.lcl_beta, rec.lcl_sebeta, rec.tss, rec.tse, rec.a1, rec.freq_a1]) 
        
        #print (introHeader)
        #print (introText)
        
        if downloadFlag == "1":
	    
            response["Content-type"] = "text/csv"
            return response          
        
        featureList1 = ['chemical','disease','gene']
        eqtlList = ['fat_p','skin_p','lcl_p']
        
    except:
        traceback.print_exc(file=sys.stdout)

    return render(request, 'lymePortal/fetchOmniDetail.html', {
            "nlpDetailRecords":nlpDetailRecords,
            "entrezId":entrezId,
            "entrezIds":entrezIds,
            "introHeader":introHeader,
            "introText":introText,
            "gene":gene,
            "feature":feature,
            "featureList1":featureList1,
            "eqtlList":eqtlList,
            "searchTerm":searchTerm, 
            "permanentLink":permanentLink,
            
        })

@login_required
def displayFileList(request):

    messages = []

    try:
        datafileMap = {settings.DATA_FOLDER + "gene_counts.tsv":"counts_matrix",settings.DATA_FOLDER + "ClinicalTraits_Lyme.csv":"clinical_data", }
    except:
        traceback.print_exc(file=sys.stdout)

    return render(request, 'lymePortal/datafileList.html', {
            "datafileMap":datafileMap,
        })

@login_required
def displayFileList(request):

    messages = []

    try:
        datafileMap = {settings.DATA_FOLDER + "gene_counts.tsv":"counts_matrix",settings.DATA_FOLDER + "ClinicalTraits_Lyme.csv":"clinical_data", }
    except:
        traceback.print_exc(file=sys.stdout)

    return render(request, 'lymePortal/datafileList.html', {
            "datafileMap":datafileMap,
        })

@login_required
def displayLimmaResults(request):

    messages = []

    try:
        
        limmaList = ["v1_case_control", "v2_case_control", "v5_case_control"]

        limmaMap = {
            "v1_case_control" : settings.DATA_FOLDER + "DE_full_results_v1_csv.csv",
            "v2_case_control" : settings.DATA_FOLDER + "DE_full_results_v2_csv.csv",
            "v5_case_control": settings.DATA_FOLDER + "DE_full_results_v5_csv.csv",
            }
        
        geneSymbolDF = pd.read_table("genelistmapping.txt",sep='\t', index_col=None)
        geneSymbolDF["entrezID"] = geneSymbolDF["entrezID"].astype(int)        
        entrezIDList = geneSymbolDF["entrezID"].tolist()
        geneSymbolList = geneSymbolDF["geneSymbol"].tolist()    
        
        geneSymbolList = [x.lower() for x in geneSymbolList]
        
        contrastObjList = []

        applyFilterButton = request.POST.get("applyFilter","0" )
        downloadFlag = request.POST.get("downloadFlag","0" )

        contrastNum = int(request.POST.get("contrastNum",0 ))
        upDownFlag = request.POST.get("upDownFlag","0" )
        
        print ("**!! download flag ="+str(downloadFlag))
        print ("**!! upDown flag ="+str(upDownFlag))
        print ("**!! contrastNum ="+str(contrastNum))

        for index, fileName in enumerate(limmaList):
            
            cutOffUpLogFC = 1.5
            cutOffUpPValue = 0
            cutOffDownLogFC = 1.5
            cutOffDownPValue = 0
            
            try:
                cutOffUpLogFC = float(request.POST.get("cutOffUpLogFC-"+ str(index),1.5 ))
                cutOffUpPValue = float(request.POST.get("cutOffUpPValue-"+ str(index),0 ))
                
                cutOffDownLogFC = float(request.POST.get("cutOffDownLogFC-" + str(index),-1.5 ))
                cutOffDownPValue = float(request.POST.get("cutOffDownPValue-" + str(index),0 ))
            
            except:
            
                pass
            
            print ("** for index ="+str(index))

            print ("cutOffUpLogFC="+str(cutOffUpLogFC))
            print ("cutOffUpPValue="+str(cutOffUpPValue))
            print ("cutOffDownLogFC="+str(cutOffDownLogFC))
            print ("cutOffDownPValue="+str(cutOffDownPValue))
            
            contrastObj = ContrastObj()
        
            contrastObj.contrast = fileName 
            
            contrastObj.cutOffUpLogFC = cutOffUpLogFC             
            contrastObj.cutOffUpPValue = cutOffUpPValue             
            contrastObj.cutOffDownLogFC = cutOffDownLogFC             
            contrastObj.cutOffDownPValue = cutOffDownPValue             
            
            filePath = limmaMap[fileName]
        
            topTableDataFrame = pd.read_csv(filePath, index_col = None)

            topTableUpDataFrame = topTableDataFrame[topTableDataFrame['logFC'] > cutOffUpLogFC]

            if cutOffUpPValue != 0:
                topTableUpDataFrame = topTableUpDataFrame[topTableUpDataFrame['P.Value'] < cutOffUpPValue]
   
            topTableDownDataFrame = topTableDataFrame[topTableDataFrame['logFC'] < cutOffDownLogFC]
    
            if cutOffDownPValue != 0:
                topTableDownDataFrame = topTableDownDataFrame[topTableDownDataFrame['P.Value'] < cutOffDownPValue]
    
            geneUpList = list(topTableUpDataFrame.sort_values('logFC',ascending = False).values)
            geneDownList = list(topTableDownDataFrame.sort_values('logFC',ascending = False).values)
            
            if downloadFlag == "1" and contrastNum == index:
                
                response = HttpResponse(content_type='text/csv')

                geneList = []
                
                if upDownFlag == "0":
                
                    geneList = geneUpList
                    
                    filename = "limma_results_" + fileName +  "_up_" + str(datetime.now()).replace("-", "")[0:8] + ".csv"

                else:

                    geneList = geneDownList

                    filename = "limma_results_" + fileName +  "_down_" + str(datetime.now()).replace("-", "")[0:8] + ".csv"
                    
                response["Content-Disposition"] = "attachment; filename=" + filename
            
                writer = csv.writer(response)
                
                writer.writerow(['Gene Symbol','Gene Id','logFC','tValue','pValue','Adj. P. Val.'])

                for geneIndex, data in enumerate ( geneList ):  
                    
                    entrezId = 0
                    
                    try:
                        entrezId = geneSymbolList.index(data[0].lower())
                    except:
                        pass
                
                    writer.writerow([ data[0], entrezId, data[1], data[2],data[3],data[4] ])
            
                response["Content-type"] = "text/csv"
                return response    

            for geneIndex, data in enumerate ( geneUpList ):
    
                geneUpObj = GeneInfoObj()
    
                if geneIndex > DEFAULT_MAX_LIST_DISPLAY:
    
                    break
    
                #print " gene id = " + str(data[0])
                geneUpObj.geneSymbol = data[0]
                
                try:
                    geneUpObj.geneId = entrezIDList [ geneSymbolList.index(data[0].lower())] 
                except:
                    pass

                geneUpObj.logFC = '{:0.4e}'.format(data[1])
                geneUpObj.tValue = '{:0.4e}'.format(data[2])
                geneUpObj.pValue = '{:0.4e}'.format(data[3])
                geneUpObj.adjustedPValue = '{:0.4e}'.format(data[4])
    
                contrastObj.geneUpObjList.append(geneUpObj)
    
            for geneIndex, data in enumerate ( geneDownList ):
    
                if geneIndex > DEFAULT_MAX_LIST_DISPLAY:
    
                    break
    
                geneDownObj = GeneInfoObj()

                geneDownObj.geneSymbol = data[0]   
                
                try:
                    geneDownObj.geneId = entrezIDList [ geneSymbolList.index(data[0].lower())] 
                except:
                    pass

                geneDownObj.logFC = '{:0.4e}'.format(data[1])
                geneDownObj.tValue = '{:0.4e}'.format(data[2])
                geneDownObj.pValue = '{:0.4e}'.format(data[3])
                geneDownObj.adjustedPValue = '{:0.4e}'.format(data[4])
                
                contrastObj.geneDownObjList.append(geneDownObj)
    
            contrastObj.geneDownObjList.sort(key=lambda x: x.adjustedPValue)
    
            for geneIndex, geneObj in enumerate ( contrastObj.geneDownObjList ):
    
                contrastObj.geneDownObjList[geneIndex].geneRank = geneIndex + 1
    
            contrastObj.geneUpObjList.sort(key=lambda x: x.adjustedPValue)
    
            for geneIndex, geneObj in enumerate ( contrastObj.geneUpObjList ):
    
                contrastObj.geneUpObjList[geneIndex].geneRank = geneIndex + 1

            customGeneList = []
    
            #contrastObj = scatterPlot(contrastObj, topTableDataFrame, customGeneList)
    
            #contrastObj = clusterMap(contrastObj, topTableDataFrame, customGeneList)
            
            contrastObjList.append(contrastObj)        
            
    except:
        traceback.print_exc(file=sys.stdout)

    return render(request, 'lymePortal/displayLimmaResults.html', {
            "contrastObjList":contrastObjList,
        })

# scatter plot

@login_required
def feedback(request):

    messages = []

    try:
        print ( " in feedback ") 
    except:
        traceback.print_exc(file=sys.stdout)

    return render(request, 'lymePortal/feedback.html', {
        })    

# scatter plot
def submitFeedback(request):

    messages = []

    try:
	
        emailAddress = request.POST.get("emailAddress","" )	
        feedback = request.POST.get("feedback","" )	
        otherFeedback = request.POST.get("otherFeedback","" )	
        feedbackQuestion = request.POST.get("feedbackQuestion","" )	
        toEmail = request.POST.get("toEmail","" )
        send_mail(
	    'LymeMIND Feedback',
	    'Feedback: '+ feedback + 'Other feedback: ' + otherFeedback + 'Question: ' + feedbackQuestion,
	    emailAddress,
	    [toEmail],
	    fail_silently=False,
	)

    except:
        traceback.print_exc(file=sys.stdout)

    return render(request, 'lymePortal/feedbackSubmitted.html', {
        })    
@login_required
def showHelp(request):
    
    try:    
    
        with open('data/LymeMIND_FAQ.pdf',  encoding = "ISO-8859-1") as pdf:

            response = HttpResponse(pdf.read())
            response['Content-Disposition'] = 'attachment;filename=LymeMIND_FAQ.pdf'
            response["Content-type"] = "application/pdf"        
            
            return response
        pdf.closed
        
    except:
        traceback.print_exc(file=sys.stdout)
    
    return      

@login_required
def showPlots(request):
    
    try:    
    
        limmaList = ["v1_case_control", "v2_case_control", "v5_case_control"]

        limmaMap = {
            "v1_case_control" : settings.DATA_FOLDER + "DE_full_results_v1_csv.csv",
            "v2_case_control" : settings.DATA_FOLDER + "DE_full_results_v2_csv.csv",
            "v5_case_control": settings.DATA_FOLDER + "DE_full_results_v5_csv.csv",
            }
        
        geneSymbolDF = pd.read_table("genelistmapping.txt",sep='\t', index_col=None)
        geneSymbolDF["entrezID"] = geneSymbolDF["entrezID"].astype(int)        
        entrezIDList = geneSymbolDF["entrezID"].tolist()
        geneSymbolList = geneSymbolDF["geneSymbol"].tolist()    
        
        geneSymbolList = [x.lower() for x in geneSymbolList]
        
        contrastObj = ()

        contrastNum = int(request.POST.get("contrastNum",0 ))
        customGeneListString = request.POST.get("customGeneList","" )
        
        cutOffUpLogFC = 1.5
        cutOffUpPValue = 0
        
        cutOffDownLogFC = 1.5
        cutOffDownPValue = 0

        try:
            cutOffUpLogFC = float(request.POST.get("cutOffUpLogFC",1.5 ))
            cutOffUpPValue = float(request.POST.get("cutOffUpPValue",0 ))
            
            cutOffDownLogFC = float(request.POST.get("cutOffDownLogFC",-1.5 ))
            cutOffDownPValue = float(request.POST.get("cutOffDownPValue",0 ))        
        except:
            pass
        
        print ("**!! customGeneList ="+str(customGeneList))
        print ("**!! contrastNum ="+str(contrastNum))
        
        customGeneList = customGeneListString.split(",")
        
        fileName = limmaList[contrastNum]
        
        contrastObj = ContrastObj()
        contrastObj.contrast = fileName

        filePath = limmaMap[fileName]
    
        topTableDataFrame = pd.read_csv(filePath, index_col = None)

        topTableUpDataFrame = topTableDataFrame[topTableDataFrame['logFC'] > cutOffUpLogFC]

        if cutOffUpPValue != 0:
            topTableUpDataFrame = topTableUpDataFrame[topTableUpDataFrame['P.Value'] < cutOffUpPValue]

        topTableDownDataFrame = topTableDataFrame[topTableDataFrame['logFC'] < cutOffDownLogFC]

        if cutOffDownPValue != 0:
            topTableDownDataFrame = topTableDownDataFrame[topTableDownDataFrame['P.Value'] < cutOffDownPValue]

        print (" ** before scatter ")
        contrastObj = scatterPlot(contrastObj, topTableDataFrame, customGeneList)
        print (" ** after scatter ")

        print (" ** before heatmap ")
        #contrastObj = clusterMap(contrastObj, topTableDataFrame, customGeneList)
        print (" ** after heatmap " + contrastObj.clusterMapName)
                
    except:
        traceback.print_exc(file=sys.stdout)
    
    return render(request, 'lymePortal/showPlots.html', {
            "contrastObj":contrastObj,
        })

# scatter plot
def scatterPlot(contrastObj, topTableDataFrame, customGeneList):

    try:
        
        print(" in scatter !!!")
        print(topTableDataFrame.columns)
        
        topTableDataFrame = topTableDataFrame.rename(columns={"Unnamed: 0":"Geneid"})
        
        normalizedCountsDf = pd.read_table(settings.DATA_FOLDER + "gene_counts.tsv", sep='\t', index_col=None)
        
        print(" counts matrix !!!")
        print(normalizedCountsDf.columns)
        
        ## 01 - cases, 02 controls
        normalizedCountsDfCases = normalizedCountsDf.filter(regex=("JHU-01.*"))
        normalizedCountsDfControls = normalizedCountsDf.filter(regex=("JHU-02.*"))
        
        normalizedCountsDf["mean_case"] = normalizedCountsDfCases.mean(axis=1)
        normalizedCountsDf["mean_control"] = normalizedCountsDfControls.mean(axis=1)

        normalizedCountsDf = normalizedCountsDf[["Geneid","mean_case","mean_control"]]

        print (normalizedCountsDf.head() )
        
        scatterDf = pd.merge(topTableDataFrame, normalizedCountsDf, how="inner", on = "Geneid")        

        #print (scatterDf.head() )

        #scatterDf = pd.DataFrame()

        cutOffUpPValue = contrastObj.cutOffUpPValue
        cutOffDownPValue = contrastObj.cutOffDownPValue

        cutOffUpLogFC = contrastObj.cutOffUpLogFC
        cutOffDownLogFC = contrastObj.cutOffDownLogFC

        numerator = "mean_case"
        denominator = "mean_control"

        scatterDf = scatterDf [  (scatterDf[denominator] !=  0 ) & (scatterDf[numerator] != 0) ]

        scatterDf[numerator] = pd.to_numeric(scatterDf[numerator], errors='coerce').fillna(0.00001).apply(log)
        scatterDf[denominator] = pd.to_numeric(scatterDf[denominator], errors='coerce').fillna(0.00001).apply(log)
        
        significantDf = scatterDf [ ( (scatterDf["logFC"] >  1.5 ) |  (scatterDf["logFC"] <  -1.5 ) ) & (scatterDf["adj.P.Val"] < 0.05) ]
        scatterDf["color"] = ['red' if  x in significantDf["Geneid"].tolist() else "blue" for x in scatterDf["Geneid"] ]
        scatterDf["marker"] = [3 if x in significantDf["Geneid"].tolist() else 1 for x in scatterDf["Geneid"] ]

        print (scatterDf.head())

        xmin, xmax = min(scatterDf[numerator].tolist() )  ,  max ( scatterDf[numerator].tolist() )
        ymin, ymax = min(scatterDf[denominator].tolist() )  ,  max ( scatterDf[denominator].tolist() )

        #topTableDataFrame.to_csv("finalDF_" + str(contrastObj.contrast.replace(".","_")) + ".csv", index = None)

        fig = plt.figure(figsize=(8, 8))

        size = fig.get_size_inches()*fig.dpi # size in pixels

        dpi = fig.get_dpi()

        scatterDfBlue = scatterDf [ scatterDf["color"] == "blue" ]

        scatterDfRed = scatterDf [ scatterDf["color"] == "red" ]

        plt.scatter(scatterDfBlue[denominator], scatterDfBlue[numerator],color='#3366ff', label=' ' , s = .85)

        plt.scatter(scatterDfRed[denominator], scatterDfRed[numerator],color='#f04722', label=' ' , s = 15)

        #customGeneList = ["PTAFR", "ADM"]

        for index, geneSymbol in enumerate(customGeneList):
            
            geneIndex = 0
            
            if geneSymbol in scatterDfBlue["Geneid"].tolist():            
                geneIndex = scatterDfBlue["Geneid"].tolist().index(geneSymbol)
                plt.annotate(geneSymbol, (scatterDfBlue[denominator].tolist()[geneIndex],scatterDfBlue[numerator].tolist()[geneIndex]), arrowprops=dict(arrowstyle="->",
                            connectionstyle="arc3"), bbox={'facecolor':'green', 'alpha':0.5, 'pad':10})
            elif geneSymbol in scatterDfRed["Geneid"].tolist():  
                geneIndex = scatterDfRed["Geneid"].tolist().index(geneSymbol)                
                plt.annotate(geneSymbol, (scatterDfRed[denominator].tolist()[geneIndex],scatterDfRed[numerator].tolist()[geneIndex]),  arrowprops=dict(arrowstyle="->",
                            connectionstyle="arc3"), bbox={'facecolor':'green', 'alpha':0.5, 'pad':10})
                
        plt.xlabel(denominator)
        plt.ylabel(numerator)

        plt.title('scatterPlot_' + numerator + '_' + denominator + "_" + contrastObj.contrast)

        xymin = min(xmin, ymin)
        xymax = max(xmax, ymax)

        plt.xlim(xymin - .5, xymax - xymin)
        plt.ylim(xymin - .5, xymax-xymin)

        axes = plt.gca()

        plt.plot(axes.get_xlim(), axes.get_ylim(), c = "black")

        plt.plot([axes.get_xlim()[0] + log(cutOffUpLogFC), axes.get_xlim()[1] + log(cutOffUpLogFC)], [axes.get_ylim()[0], axes.get_ylim()[1]], c = "black")
        plt.plot([axes.get_xlim()[0], axes.get_xlim()[1] ], [axes.get_ylim()[0] + log(cutOffUpLogFC), axes.get_ylim()[1] + log(cutOffUpLogFC)], c = "black")

        axes.spines['top'].set_visible(False)
        axes.spines['right'].set_visible(False)

        axes.spines["top"].set_linewidth(2)
        axes.spines["right"].set_linewidth(2)

        axes.yaxis.set_ticks_position('left')
        axes.xaxis.set_ticks_position('bottom')

        plotPath = settings.IMAGE_FOLDER   + '/plots/scatterPlot_' + numerator + '_' + denominator+ "_" + contrastObj.contrast
        plotName = 'scatterPlot_' + numerator + '_' + denominator+ "_" + contrastObj.contrast

        plt.savefig(plotPath + ".png")

        contrastObj.scatterPlotPath = plotPath
        contrastObj.scatterPlotName = plotName

    except:

        traceback.print_exc(file=sys.stdout)

    return contrastObj

# clustermap / heatmap
def clusterMap(contrastObj, topTableDataFrame, customGeneList):

    try:

        cutOffUpPValue = contrastObj.cutOffUpPValue
        cutOffDownPValue = contrastObj.cutOffDownPValue

        cutOffUpLogFC = contrastObj.cutOffUpLogFC
        cutOffDownLogFC = contrastObj.cutOffDownLogFC

        if cutOffUpLogFC == 0:
            cutOffUpLogFC = 1.5

        if cutOffDownLogFC == 0:
            cutOffDownLogFC = -1.5

        #if len(customGeneList) > 0:

            #topTableDataFrame = topTableDataFrame[topTableDataFrame["genes"].isin(customGeneList)]

        #else:

            #topTableDataFrame = topTableDataFrame[(topTableDataFrame['logFC'] > cutOffUpLogFC) | (topTableDataFrame['logFC'] < cutOffDownLogFC)]

        topTableDataFrame = topTableDataFrame.rename(columns={"Unnamed: 0":"Geneid"})
        
        topTableDataFrame = topTableDataFrame [ ( (topTableDataFrame["logFC"] >  1.5 ) |  (topTableDataFrame["logFC"] <  -1.5 ) ) & (topTableDataFrame["adj.P.Val"] < 0.05) ]
        
        topTableDataFrame = topTableDataFrame.sort_values('adj.P.Val',ascending = False)
        
        topTableDataFrame = topTableDataFrame[:15]

        normalizedCountsDf = pd.read_table(settings.DATA_FOLDER + "gene_counts.tsv", sep='\t', index_col=None)
        
        print(" counts matrix !!!")
        print(normalizedCountsDf.columns)
        
        normalizedCountsDf.dropna()
        
        print (normalizedCountsDf.head() )
        
        clusterDf = normalizedCountsDf[normalizedCountsDf["Geneid"].isin(topTableDataFrame["Geneid"]).tolist()]

        clusterDf.dropna()

        clusterDf = clusterDf.set_index(["Geneid"])
        
        #clusterDf.columns.name = 'Sample1'             
        #clusterDf = clusterDf [clusterDf.columns[1:] ]
        
        #clusterDf[clusterDf.columns[1:]] = clusterDf[clusterDf.columns[1:]].astype(float)        

        #clusterDf = clusterDf[clusterDf.columns[1:]].apply(math.log)
        
        print ()

        print(" min == " + str(clusterDf.min()))

        #ax = sns.clustermap(clusterDf.corr().fillna(0), z_score = 1)

        #ax = sns.clustermap(clusterDf.fillna(0), z_score = 1)

        #sns.plt.tight_layout()
        ax = sns.heatmap(clusterDf, norm=colors.LogNorm(vmin=clusterDf.min(), vmax=clusterDf.max()),
                   cmap='PuBu_r')

        #sns.plt.setp(ax.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
        #sns.plt.setp(ax.ax_heatmap.xaxis.get_majorticklabels(), rotation=90)

        ax.set_title('Cluster map for ' + str(contrastObj.contrast))

        #ax.set_xticks(rotation = 90) 
        #ax.set_yticks(rotation = 90) 

        plotName = "clusterMap" + "_"+contrastObj.contrast
        plotPath = settings.IMAGE_FOLDER + "/plots/" + plotName +".png"

        contrastObj.clusterMapName = plotName
        
        print ("contrastObj.clusterMapName=" + str(contrastObj.clusterMapName))

        if os.path.isfile(plotPath):
            os.remove(plotPath)

        ax.get_figure().savefig(plotPath)

        fig = plt.figure(figsize=(18, 18), dpi = 1200)

        size = fig.get_size_inches()*fig.dpi # size in pixels

        dpi = fig.get_dpi()
        
        #source = ColumnDataSource(clusterDf)
        
        #TOOLS = "hover,save,pan,box_zoom,reset,wheel_zoom"
        
        #columnNumbers = [x.split("-")[2] for x in clusterDf.columns]
        
        #p = figure(title="Gene_Expression_" + contrastObj.contrast,
                   #x_range = columnNumbers, y_range=clusterDf.index.tolist(),
                   #plot_width=900, plot_height=400,
                   #tools=TOOLS, toolbar_location='below')
        
        #p.rect(x="Geneid", y="Sample1", width=1, height=1,
               #source=source,
               ##fill_color={'field': 'rate', 'transform': mapper},
               #line_color=None)
        
        #p.grid.grid_line_color = None
        #p.xaxis.visible = None
        #p.yaxis.visible = None

        #plotScript, plotDiv = components(p)

        #print (" plotScript = " + str(plotScript) )
        #print (" plotDiv = " + str(plotDiv) )

        #contrastObj.bokehHeatMapPlotScript = plotScript
        #contrastObj.bokehHeatMapPlotDiv = plotDiv        
        
    except:

        traceback.print_exc(file=sys.stdout)

    return contrastObj
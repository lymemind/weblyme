import pandas as pd
import numpy as np
import os
import sys
import traceback
import math
import seaborn as sns
from collections import OrderedDict
#from bokeh.charts import Bar, output_file, show
import matplotlib.pyplot as plt

def createMergedDF(fileName1, fileName2, database, direction):

    '''
    plot results from Limma analysis
    Input:
    Output:
    '''

    geneDBMap = {}

    try:

        print ( " before 1" )
        geneDF1 = getGeneDF(fileName1, database, direction)

        print ( " before 2" )
        geneDF2 = getGeneDF(fileName2, database, direction)

        print ( " before merge" )
        pd.merge(geneDF1, geneDF2, on='gene', how='outer')

        geneDFFinal = geneDF1.merge(geneDF2, on = ["gene","pathway"])

        geneDFFinal = geneDFFinal.rename(columns={"count_x":"count_v1", "count_y":"count_v2"})
        
        #geneSymbolDF = pd.read_table("genelistmapping.txt",sep='\t', index_col=None)
        #geneSymbolDF["entrezID"] = geneSymbolDF["entrezID"].astype(int)        
        #entrezIDList = geneSymbolDF["entrezID"].tolist()
        #geneSymbolList = geneSymbolDF["geneSymbol"].tolist()
        
        #geneDFFinal["geneSymbol"] = [geneSymbolList [ entrezIDList.index(int(x))] for x in geneDFFinal["gene"].tolist()]
        
        geneDFFinal = geneDFFinal.sort_values(['count_v1','count_v2'], ascending=[False,False])
        
        if len(geneDFFinal) > 50:
            geneDFFinal = geneDFFinal[:50]
        
        print (geneDFFinal.head())

    except:
        traceback.print_exc(file=sys.stdout)

    return geneDFFinal

def getGeneDF(fileName, database, direction):

    '''
    plot results from Limma analysis
    Input:
    Output:
    '''

    print ( " fileName " + str(fileName) + " database " + str(database) + " direction " + str(direction) )

    geneDBMap = {}

    dfFinal1 = pd.DataFrame()

    try:

        df1 = pd.read_table(fileName,sep='\t', index_col=None)

        df1 = df1[[".id", "module", "Adjusted.Pvalue", "feature", "enrichment_overlap"]]

        df1 = df1.rename(columns={".id":"db", "module":"pathway","Adjusted.Pvalue":"adj_P_Val", "feature":"direction", "enrichment_overlap":"geneset"})

        df1 = df1[(df1["db"] == database) & (df1["direction"] == direction)]

        df1['genes'] = df1['geneset'].str.split("|")

        print ( df1.head())

        for index, row in df1.iterrows():

            for gene in row["genes"]:

                if gene not in geneDBMap:

                    geneDBMap[gene] = {}

                if row["pathway"] not in geneDBMap[gene]:

                    geneDBMap[gene][row['pathway']] = 0

                geneDBMap[gene][row["pathway"]] = np.round(-1 * np.log10(row["adj_P_Val"]), 4)

        geneCountList = []

        tempList = []

        [  [ tempList.append([k1, k2, v2]) for k2, v2 in v1.items()] for k1,v1 in geneDBMap.items() ]

        print ( tempList)

        dfFinal1 =  pd.DataFrame(tempList)

        dfFinal1 = dfFinal1.rename(columns={0:"gene", 1:"pathway",2:"count"})

        #print ( dfFinal1.head() )

    except:
        traceback.print_exc(file=sys.stdout)

    return dfFinal1

def fetchGeo(geoId):
    try:
        gse = GEOparse.get_GEO(geo=geoId, destdir="./")
        #print("GSM example:")
    
        for gsm_name, gsm in gse.gsms.items():
            print("Name: ", gsm_name)
            print("Metadata:",)
            for key, value in gsm.metadata.items():
                print(" - %s : %s" % (key, ", ".join(value)))
            print ("Table data:",)
            print (gsm.table.head())
            
            break
        
        print()
        print("GPL example:")
        
        for gpl_name, gpl in gse.gpls.items():
            
            print("Name: ", gpl_name)
            print("Metadata:",)
            
            for key, value in gpl.metadata.items():
                print(" - %s : %s" % (key, ", ".join(value)))
            
            print("Table data:",)
            print(gpl.table.head())
            print (len(gpl.table))
            
            break
    except:
        traceback.print_exc(file=sys.stdout)

    return